/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.mapper.tools;

import org.eclipse.openk.gridfailureinformation.constants.Constants;
import org.eclipse.openk.gridfailureinformation.model.HtblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class AddressSplitterMergerTest {
    @Test
    void shouldSplitFreetextAddressCorrectly() {
        FailureInformationDto dto = new FailureInformationDto();

        AddressSplitterMerger.splitFreetextAddress(Constants.FREETEXT_ADDRESS_TYPE, "City", "district", "postcode", dto);
        assertEquals("City", dto.getFreetextCity());
        assertEquals( "district", dto.getFreetextDistrict());
        assertEquals( "postcode", dto.getFreetextPostcode());
    }

    @Test
    void shouldmergeFreetextAddressCorrectly() {
        FailureInformationDto dto = new FailureInformationDto();
        TblFailureInformation tbl = new TblFailureInformation();

        dto.setFreetextCity("city");
        dto.setFreetextDistrict("district");
        dto.setFreetextPostcode("postcode");
        AddressSplitterMerger.mergeFreetextAddress(dto, tbl);

        assertEquals( "city", tbl.getCity());
        assertEquals( "district", tbl.getDistrict());
        assertEquals( "postcode", tbl.getPostcode());
    }

    @Test
    void shouldmergeFreetextAddressCorrectly2() {
        FailureInformationDto dto = new FailureInformationDto();
        TblFailureInformation tbl = new TblFailureInformation();

        AddressSplitterMerger.mergeFreetextAddress(dto, tbl);

        assertNull( tbl.getAddressType());
    }


    @Test
    void shouldmergeFreetextAddressHistoryCorrectly() {
        FailureInformationDto dto = new FailureInformationDto();
        HtblFailureInformation htbl = new HtblFailureInformation();

        dto.setFreetextCity("city");
        dto.setFreetextDistrict("district");
        dto.setFreetextPostcode("postcode");
        AddressSplitterMerger.mergeFreetextAddress(dto, htbl);

        assertEquals( "city", htbl.getCity());
        assertEquals( "district", htbl.getDistrict());
        assertEquals( "postcode", htbl.getPostcode());
    }


    @Test
    void shouldmergeFreetextAddressHistoryCorrectly2() {
        FailureInformationDto dto = new FailureInformationDto();
        HtblFailureInformation tbl = new HtblFailureInformation();

        AddressSplitterMerger.mergeFreetextAddress(dto, tbl);

        assertNull( tbl.getAddressType());
    }
}
