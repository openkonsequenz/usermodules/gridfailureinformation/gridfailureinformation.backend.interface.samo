/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.exceptions.NotFoundException;
import org.eclipse.openk.gridfailureinformation.model.TblDistributionGroup;
import org.eclipse.openk.gridfailureinformation.repository.DistributionGroupRepository;
import org.eclipse.openk.gridfailureinformation.service.DistributionGroupMemberService;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionGroupMemberDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
class DistributionGroupMemberControllerTest {
    @Autowired
    private DistributionGroupRepository distributionGroupRepository;

    @Autowired
    @SpyBean
    private DistributionGroupMemberService distributionGroupMemberService;

    private static MockMvc mockMvc;

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(new DistributionGroupMemberController(distributionGroupMemberService))
                .build();
    }

    @Test
    void shouldReturnDistributionGroupMembers() throws Exception {
        List<DistributionGroupMemberDto> distributionGroupMemberDtoList = MockDataHelper.mockDistributionGroupMemberDtoList();

        doReturn(distributionGroupMemberDtoList).when(distributionGroupMemberService).getDistributionGroupMembers();

        mockMvc.perform(get("/distribution-groups/members"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void shouldReturnARequestedMember() throws Exception {
        DistributionGroupMemberDto memberDto = MockDataHelper.mockDistributionGroupMemberDto();

        doReturn(memberDto).when(distributionGroupMemberService).getMemberByUuid(any(UUID.class), any(UUID.class));

        mockMvc.perform(get("/distribution-groups/{groupUuid}/members/{uuid}", UUID.randomUUID(), UUID.randomUUID() ))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("contactId", is(  memberDto.getContactId().toString())));
    }


    @Test
    void shouldReturnMembersOfASingleGroup() throws Exception {
        List<DistributionGroupMemberDto> distributionGroupMemberDtoList = MockDataHelper.mockDistributionGroupMemberDtoList();

        doReturn(distributionGroupMemberDtoList).when(distributionGroupMemberService).getMembersByGroupId(any(UUID.class));

        mockMvc.perform(get("/distribution-groups/" + UUID.randomUUID() + "/members"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void shouldExceptionIfNotFoundDistributionGroup() throws Exception {
        doThrow(new NotFoundException()).when(distributionGroupMemberService).getMembersByGroupId(any(UUID.class));

        mockMvc.perform(get("/distribution-groups/" + UUID.randomUUID() + "/members"))
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldInsertDistributionGroupMember() throws Exception {
        DistributionGroupMemberDto memberDto = MockDataHelper.mockDistributionGroupMemberDto();
        TblDistributionGroup tblDistributionGroup = MockDataHelper.mockTblDistributionGroup();

        when(distributionGroupRepository.findByUuid(any())).thenReturn(Optional.of(tblDistributionGroup));
        doReturn(memberDto).when(distributionGroupMemberService).insertDistributionGroupMember(any(), any());

        mockMvc.perform(post("/distribution-groups/{groupUuid}/members/", memberDto.getDistributionGroupUuid().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(memberDto)))
                .andExpect(jsonPath("$.contactId", is(memberDto.getContactId().toString())))
                .andExpect(status().is2xxSuccessful());
    }


    @Test
    void shouldUpdateDistributionGroupMember() throws Exception {
        DistributionGroupMemberDto memberDto = MockDataHelper.mockDistributionGroupMemberDto();
        doReturn(memberDto).when(distributionGroupMemberService).updateGroupMember(any(UUID.class), any(DistributionGroupMemberDto.class));

        mockMvc.perform(put("/distribution-groups/{groupUuid}/members/{uuid}", memberDto.getDistributionGroupUuid().toString(), memberDto.getUuid().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(memberDto)))
                .andExpect(status().is2xxSuccessful());
    }


    @Test
    void shouldUpdateDistributionGroupMemberDueToException() throws Exception {
        DistributionGroupMemberDto memberDto = MockDataHelper.mockDistributionGroupMemberDto();

        doReturn(memberDto).when(distributionGroupMemberService).updateGroupMember(any(UUID.class), any(DistributionGroupMemberDto.class));

        mockMvc.perform(put("/distribution-groups/{groupUuid}/members/{uuid}", UUID.randomUUID().toString(), UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(memberDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldDeleteDistributionGroupMember() throws Exception {
        doNothing().when(distributionGroupMemberService).deleteDistributionGroupMember(any(UUID.class), any(UUID.class));

        mockMvc.perform(delete("/distribution-groups/{groupUuid}/members/{uuid}", UUID.randomUUID().toString(), UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
    }


    @Test
    void shouldDownloadMembersByUuidAsCSV() throws Exception {
        ResponseEntity<Resource> responseEntity = MockDataHelper.mockResponseEntity();

        doReturn(responseEntity).when(distributionGroupMemberService).handleLoadFile(any(UUID.class));

        mockMvc.perform(get("/distribution-groups/{groupUuid}/members/csv",  UUID.randomUUID().toString()))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType("text/csv"));
    }
}
