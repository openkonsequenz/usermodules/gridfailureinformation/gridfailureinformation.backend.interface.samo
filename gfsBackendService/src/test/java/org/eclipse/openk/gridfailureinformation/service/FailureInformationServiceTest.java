/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.ProcessHelper;
import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.constants.Constants;
import org.eclipse.openk.gridfailureinformation.exceptions.BadRequestException;
import org.eclipse.openk.gridfailureinformation.exceptions.NotFoundException;
import org.eclipse.openk.gridfailureinformation.exceptions.OperationDeniedException;
import org.eclipse.openk.gridfailureinformation.model.HtblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.RefBranch;
import org.eclipse.openk.gridfailureinformation.model.RefExpectedReason;
import org.eclipse.openk.gridfailureinformation.model.RefFailureClassification;
import org.eclipse.openk.gridfailureinformation.model.RefRadius;
import org.eclipse.openk.gridfailureinformation.model.RefStatus;
import org.eclipse.openk.gridfailureinformation.model.TblAddress;
import org.eclipse.openk.gridfailureinformation.model.TblDistributionGroup;
import org.eclipse.openk.gridfailureinformation.model.TblFailinfoStation;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformationDistributionGroup;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformationPublicationChannel;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformationReminderMailSent;
import org.eclipse.openk.gridfailureinformation.model.TblStation;
import org.eclipse.openk.gridfailureinformation.repository.AddressRepository;
import org.eclipse.openk.gridfailureinformation.repository.BranchRepository;
import org.eclipse.openk.gridfailureinformation.repository.DistributionGroupRepository;
import org.eclipse.openk.gridfailureinformation.repository.ExpectedReasonRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureClassificationRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationDistributionGroupRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationPublicationChannelRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationReminderMailSentRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationStationRepository;
import org.eclipse.openk.gridfailureinformation.repository.HistFailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.RadiusRepository;
import org.eclipse.openk.gridfailureinformation.repository.StationRepository;
import org.eclipse.openk.gridfailureinformation.repository.StatusRepository;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationLastModDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationPublicationChannelDto;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@Log4j2
@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
class FailureInformationServiceTest {
    @Autowired
    private FailureInformationService failureInformationService;

    @Autowired
    private FailureInformationRepository failureInformationRepository;

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private FailureClassificationRepository failureClassificationRepository;

    @Autowired
    private StatusRepository statusRepository;

    @Autowired
    private HistFailureInformationRepository histFailureInformationRepository;

    @Autowired
    private RadiusRepository radiusRepository;

    @Autowired
    private ExpectedReasonRepository expectedReasonRepository;

    @Autowired
    private StationRepository stationRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private DistributionGroupRepository distributionGroupRepository;

    @Autowired
    private FailureInformationPublicationChannelRepository failureInformationPublicationChannelRepository;

    @Autowired
    private FailureInformationDistributionGroupRepository failureInformationDistributionGroupRepository;

    @Autowired
    private FailureInformationReminderMailSentRepository failureInformationReminderMailSentRepository;

    @Autowired
    private FailureInformationStationRepository failureInformationStationRepository;

    @Autowired
    private ProcessHelper processHelper;

    @Test
    void shouldFindFailureInformations() {
        Page<TblFailureInformation> mockfailurePage = MockDataHelper.mockTblFailureInformationPage();
        RefStatus refStatus = MockDataHelper.mockRefStatusCreated();

        when(failureInformationRepository.findByTblFailureInformationForDisplay(anyLong(), anyLong(), any(Date.class), any(Pageable.class))).thenReturn(mockfailurePage);
        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refStatus));

        Page<FailureInformationDto> retPage = failureInformationService.findFailureInformations(Pageable.unpaged());

        assertEquals( mockfailurePage.getTotalElements(), retPage.getTotalElements() );
        assertEquals( mockfailurePage.getContent().get(0).getResponsibility(), retPage.getContent().get(0).getResponsibility());
    }

    @Test
    void shouldFindASingleFailureInformation() {
        TblFailureInformation mockFailure = MockDataHelper.mockTblFailureInformation();
        TblStation tblStation = MockDataHelper.mockTblStation();
        List<TblAddress> addressList = MockDataHelper.mockTblAddressList();
        RefStatus refStatus = MockDataHelper.mockRefStatusCreated();

        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refStatus));
        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(mockFailure));
        when(stationRepository.findByStationId(anyString())).thenReturn(Optional.of(tblStation));
        when(addressRepository.findByStationId(anyString())).thenReturn(addressList);

        FailureInformationDto retDto = failureInformationService.findFailureInformation(UUID.randomUUID());

        assertEquals( mockFailure.getResponsibility(), retDto.getResponsibility() );
        assertEquals( mockFailure.getUuid(), retDto.getUuid());
    }

    @Test
    void shouldFindASingleFailureInformationWithoutExternalStatus_NoStartdateNoEnddate() {
        TblFailureInformation mockFailure = MockDataHelper.mockTblFailureInformation();
        mockFailure.setFailureBegin(null);
        mockFailure.setFailureEndPlanned(null);
        TblStation tblStation = MockDataHelper.mockTblStation();
        List<TblAddress> addressList = MockDataHelper.mockTblAddressList();
        RefStatus refStatus = MockDataHelper.mockRefStatusCreated();

        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refStatus));
        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(mockFailure));
        when(stationRepository.findByStationId(anyString())).thenReturn(Optional.of(tblStation));
        when(addressRepository.findByStationId(anyString())).thenReturn(addressList);

        FailureInformationDto retDto = failureInformationService.findFailureInformation(UUID.randomUUID());

        assertEquals( mockFailure.getResponsibility(), retDto.getResponsibility() );
        assertEquals( mockFailure.getUuid(), retDto.getUuid());
        assertEquals("", retDto.getStatusExtern());
    }

    @Test
    void shouldThrowExceptionWhenFailureNotFound() {
        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.empty());
        assertThrows(NotFoundException.class,
                () -> failureInformationService.findFailureInformation(UUID.randomUUID()));
    }

    @Test
    void shouldStoreFailureInformation() {
        FailureInformationDto fiDto = MockDataHelper.mockFailureInformationDto();
        List<UUID> stationList = new ArrayList<>();
        stationList.add(UUID.fromString("8a5fb5ca-d26d-11ea-87d0-0242ac130003"));
        stationList.add(UUID.fromString("8a5fb2aa-d26d-11ea-87d0-0242ac130003"));
        stationList.add(UUID.fromString("a1375ec8-d26e-11ea-87d0-0242ac130003"));

        fiDto.setStationIds(stationList);

        TblFailureInformation fiTbl = MockDataHelper.mockTblFailureInformation();
        RefFailureClassification refFailureClassification = MockDataHelper.mockRefFailureClassification();
        RefStatus refStatus = MockDataHelper.mockRefStatusCreated();
        RefBranch refBranch = MockDataHelper.mockRefBranch();
        RefRadius refRadius = MockDataHelper.mockRefRadius();
        RefExpectedReason refExpectedReason = MockDataHelper.mockRefExpectedReason();
        List<HtblFailureInformation> hFailureList = MockDataHelper.mockHistTblFailureInformationList();

        TblStation tblStation = MockDataHelper.mockTblStation();
        TblStation tblStation2 = MockDataHelper.mockTblStation2();
        TblStation tblStation3 = MockDataHelper.mockTblStation3();

        //TblDistributionGroup tblDistributionGroup = Mock.mockTblD

        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(fiTbl));
        when(branchRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refBranch));
        when(failureClassificationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refFailureClassification));
        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refStatus));
        when(radiusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refRadius));
        when(expectedReasonRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refExpectedReason));
        when(histFailureInformationRepository.findByUuid(any(UUID.class))).thenReturn(hFailureList);
        when(stationRepository.findByUuid(eq(UUID.fromString("8a5fb2aa-d26d-11ea-87d0-0242ac130003")))).thenReturn(Optional.of(tblStation));
        when(stationRepository.findByUuid(eq(UUID.fromString("8a5fb5ca-d26d-11ea-87d0-0242ac130003")))).thenReturn(Optional.of(tblStation2));
        when(stationRepository.findByUuid(eq(UUID.fromString("a1375ec8-d26e-11ea-87d0-0242ac130003")))).thenReturn(Optional.of(tblStation3));

        when(failureInformationRepository.save(any(TblFailureInformation.class)))
                .then((Answer<TblFailureInformation>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblFailureInformation) args[0];
                });

        FailureInformationDto savedDto = failureInformationService.storeFailureInfo(fiDto, GfiProcessState.NEW);

        assertEquals(3L, savedDto.getVersionNumber());
        assertEquals(fiTbl.getResponsibility(), savedDto.getResponsibility());
        assertEquals(fiTbl.getVoltageLevel(), savedDto.getVoltageLevel());
        assertEquals(fiTbl.getPressureLevel(), savedDto.getPressureLevel());
        assertEquals(fiTbl.getFailureBegin(), savedDto.getFailureBegin());
        assertEquals(fiTbl.getFailureEndPlanned(), savedDto.getFailureEndPlanned());
        assertEquals(fiTbl.getFailureEndResupplied(), savedDto.getFailureEndResupplied());
        assertEquals(fiTbl.getPostcode(), savedDto.getPostcode());
        assertEquals(fiTbl.getCity(), savedDto.getCity());
        assertEquals(fiTbl.getDistrict(), savedDto.getDistrict());
        assertEquals(fiTbl.getStreet(), savedDto.getStreet());
        assertEquals(fiTbl.getHousenumber(), savedDto.getHousenumber());
        assertEquals(fiTbl.getStationDescription(), savedDto.getStationDescription());
        assertEquals(fiTbl.getStationCoords(), savedDto.getStationCoords());
        assertEquals(fiTbl.getRefRadius().getRadius(), savedDto.getRadius());
        assertEquals(fiTbl.getLongitude(), savedDto.getLongitude());
        assertEquals(fiTbl.getLatitude(), savedDto.getLatitude());
        assertEquals(fiTbl.getRefFailureClassification().getUuid(), savedDto.getFailureClassificationId());
        assertEquals(fiTbl.getRefFailureClassification().getClassification(), savedDto.getFailureClassification());
        assertEquals(fiTbl.getRefStatusIntern().getStatus(), savedDto.getStatusIntern());
        assertEquals(fiTbl.getRefStatusIntern().getStatus(), savedDto.getStatusIntern());
        assertEquals(fiTbl.getRefBranch().getUuid(), savedDto.getBranchId());
        assertEquals(fiTbl.getRefBranch().getName(), savedDto.getBranch());
        assertEquals(fiTbl.getRefBranch().getColorCode(), savedDto.getBranchColorCode());
        assertEquals(fiTbl.getRefExpectedReason().getUuid(), savedDto.getExpectedReasonId());
        assertEquals(fiTbl.getRefExpectedReason().getText(), savedDto.getExpectedReasonText());
    }

    @Test
    void shouldUpdateFailureInformation_emptyReferences() {
        RefStatus refStatus = MockDataHelper.mockRefStatusCreated();
        refStatus.setId(GfiProcessState.PLANNED.getStatusValue());

        TblStation tblStation = MockDataHelper.mockTblStation();
        TblStation tblStation2 = MockDataHelper.mockTblStation2();
        TblStation tblStation3 = MockDataHelper.mockTblStation3();

        List<TblAddress> addressList = MockDataHelper.mockTblAddressList();
        TblDistributionGroup tblDistributionGroup = MockDataHelper.mockTblDistributionGroup();

        List<HtblFailureInformation> listHistFailureInfos = MockDataHelper.mockHistTblFailureInformationList();

        FailureInformationDto fiDto = MockDataHelper.mockFailureInformationDto();
        fiDto.setBranchId(null);
        fiDto.setFailureClassificationId(null);
        fiDto.setFailureTypeId(null);
        fiDto.setStatusInternId(UUID.randomUUID());
        fiDto.setRadiusId(null);
        fiDto.setExpectedReasonId(null);

        List<UUID> stationList = new ArrayList<>();
        stationList.add(UUID.fromString("8a5fb5ca-d26d-11ea-87d0-0242ac130003"));
        stationList.add(UUID.fromString("8a5fb2aa-d26d-11ea-87d0-0242ac130003"));
        stationList.add(UUID.fromString("a1375ec8-d26e-11ea-87d0-0242ac130003"));

        fiDto.setStationIds(stationList);

        TblFailureInformation fiTbl = MockDataHelper.mockTblFailureInformation();
        fiTbl.setId(777L);
        fiTbl.setRefFailureClassification(null);
        fiTbl.setRefRadius(null);
        fiTbl.setRefExpectedReason(null);
        //fiTbl.setDistributionGroups(null);
        fiTbl.setStationId(null);

        when(statusRepository.findByUuid(any())).thenReturn(Optional.of(refStatus));
        when(failureInformationRepository.findByUuid(any())).thenReturn(Optional.of(fiTbl));
        when(stationRepository.findByStationId(anyString())).thenReturn(Optional.of(tblStation));
        when(stationRepository.findByUuid(eq(UUID.fromString("8a5fb2aa-d26d-11ea-87d0-0242ac130003")))).thenReturn(Optional.of(tblStation));
        when(stationRepository.findByUuid(eq(UUID.fromString("8a5fb5ca-d26d-11ea-87d0-0242ac130003")))).thenReturn(Optional.of(tblStation2));
        when(stationRepository.findByUuid(eq(UUID.fromString("a1375ec8-d26e-11ea-87d0-0242ac130003")))).thenReturn(Optional.of(tblStation3));
        when(addressRepository.findByStationId(anyString())).thenReturn(addressList);

        when(failureInformationRepository.save(any(TblFailureInformation.class)))
                .then((Answer<TblFailureInformation>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblFailureInformation) args[0];
                });
        when(histFailureInformationRepository.findByUuid(any(UUID.class))).thenReturn(listHistFailureInfos);
        when(distributionGroupRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblDistributionGroup));

        FailureInformationDto savedDto = processHelper.updateFailureInfo(fiDto);

        assertEquals(fiDto.getUuid(), savedDto.getUuid());
        assertEquals(listHistFailureInfos.size() + 1, savedDto.getVersionNumber());
        assertEquals(fiTbl.getResponsibility(), savedDto.getResponsibility());
        assertEquals(fiTbl.getVoltageLevel(), savedDto.getVoltageLevel());
        assertEquals(fiTbl.getPressureLevel(), savedDto.getPressureLevel());
        assertEquals(fiTbl.getFailureBegin(), savedDto.getFailureBegin());
        assertEquals(fiTbl.getFailureEndPlanned(), savedDto.getFailureEndPlanned());
        assertEquals(fiTbl.getFailureEndResupplied(), savedDto.getFailureEndResupplied());
        assertEquals(fiTbl.getPostcode(), savedDto.getPostcode());
        assertEquals(fiTbl.getCity(), savedDto.getCity());
        assertEquals(fiTbl.getDistrict(), savedDto.getDistrict());
        assertEquals(fiTbl.getStreet(), savedDto.getStreet());
        assertEquals(fiTbl.getHousenumber(), savedDto.getHousenumber());
        assertEquals(fiTbl.getStationDescription(), savedDto.getStationDescription());
        assertEquals(fiTbl.getStationCoords(), savedDto.getStationCoords());
        assertEquals(fiTbl.getLongitude(), savedDto.getLongitude());
        assertEquals(fiTbl.getLatitude(), savedDto.getLatitude());
//        assertEquals(5, savedDto.getAddressPolygonPoints().size()); _fd reverseMapping of stations
    }

    @Test
    void shouldNotStoreFailureInformation_Exception_FailureInformationNotFound() {
        FailureInformationDto fiDto = MockDataHelper.mockFailureInformationDto();
        RefFailureClassification refFailureClassification = MockDataHelper.mockRefFailureClassification();
        RefStatus refStatus = MockDataHelper.mockRefStatusCreated();
        RefBranch refBranch = MockDataHelper.mockRefBranch();

        //failureInformation cannot be found
        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.empty());

        when(branchRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refBranch));
        when(failureClassificationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refFailureClassification));
        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refStatus));

        when(failureInformationRepository.save(any(TblFailureInformation.class)))
                .then((Answer<TblFailureInformation>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblFailureInformation) args[0];
                });

        assertThrows(NotFoundException.class, () -> failureInformationService.storeFailureInfo(fiDto, GfiProcessState.NEW));
    }

    @Test
    void shouldNotUpdateFailureInformation_Exception_BranchNotFound() {
        FailureInformationDto fiDto = MockDataHelper.mockFailureInformationDto();
        TblFailureInformation fiTbl = MockDataHelper.mockTblFailureInformation();
        RefFailureClassification refFailureClassification = MockDataHelper.mockRefFailureClassification();
        RefStatus refStatus = MockDataHelper.mockRefStatusCreated();


        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(fiTbl));
        //branch cannot be found
        when(branchRepository.findByUuid(any(UUID.class))).thenReturn(Optional.empty());
        when(failureClassificationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refFailureClassification));
        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refStatus));

        when(failureInformationRepository.save(any(TblFailureInformation.class)))
                .then((Answer<TblFailureInformation>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblFailureInformation) args[0];
                });

        assertThrows(NotFoundException.class, () -> failureInformationService.storeFailureInfo(fiDto, GfiProcessState.NEW));

    }

    @Test
    void shouldNotUpdateFailureInformation_Exception_FailureClassificationNotFound() {
        FailureInformationDto fiDto = MockDataHelper.mockFailureInformationDto();
        TblFailureInformation fiTbl = MockDataHelper.mockTblFailureInformation();
        RefStatus refStatus = MockDataHelper.mockRefStatusCreated();
        RefBranch refBranch = MockDataHelper.mockRefBranch();


        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(fiTbl));
        when(branchRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refBranch));
        //failureClassification cannot be found
        when(failureClassificationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.empty());
        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refStatus));

        when(failureInformationRepository.save(any(TblFailureInformation.class)))
                .then((Answer<TblFailureInformation>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblFailureInformation) args[0];
                });

        assertThrows(NotFoundException.class, () -> failureInformationService.storeFailureInfo(fiDto, GfiProcessState.NEW));

    }

    @Test
    void shouldNotUpdateFailureInformation_Exception_FailureTypeNotFound() {
        FailureInformationDto fiDto = MockDataHelper.mockFailureInformationDto();
        TblFailureInformation fiTbl = MockDataHelper.mockTblFailureInformation();
        RefFailureClassification refFailureClassification = MockDataHelper.mockRefFailureClassification();
        RefStatus refStatus = MockDataHelper.mockRefStatusCreated();
        RefBranch refBranch = MockDataHelper.mockRefBranch();

        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(fiTbl));
        when(branchRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refBranch));
        when(failureClassificationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refFailureClassification));
        //failureType cannot be found
        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refStatus));

        when(failureInformationRepository.save(any(TblFailureInformation.class)))
                .then((Answer<TblFailureInformation>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblFailureInformation) args[0];
                });

        assertThrows(NotFoundException.class, () -> failureInformationService.storeFailureInfo(fiDto, GfiProcessState.NEW));

    }

    @Test
    void shouldNotUpdateFailureInformation_Exception_StatusNotFound() {
        FailureInformationDto fiDto = MockDataHelper.mockFailureInformationDto();
        TblFailureInformation fiTbl = MockDataHelper.mockTblFailureInformation();
        RefFailureClassification refFailureClassification = MockDataHelper.mockRefFailureClassification();
        RefBranch refBranch = MockDataHelper.mockRefBranch();

        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(fiTbl));
        when(branchRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refBranch));
        when(failureClassificationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refFailureClassification));
        //status cannot be found
        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.empty());

        when(failureInformationRepository.save(any(TblFailureInformation.class)))
                .then((Answer<TblFailureInformation>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblFailureInformation) args[0];
                });

        assertThrows(NotFoundException.class, () -> failureInformationService.storeFailureInfo(fiDto, GfiProcessState.NEW));

    }

    @Test
    void shouldNotUpdateFailureInformation_Exception_RadiusNotFound() {
        FailureInformationDto fiDto = MockDataHelper.mockFailureInformationDto();
        TblFailureInformation fiTbl = MockDataHelper.mockTblFailureInformation();
        RefFailureClassification refFailureClassification = MockDataHelper.mockRefFailureClassification();
        RefStatus refStatus = MockDataHelper.mockRefStatusCreated();
        RefBranch refBranch = MockDataHelper.mockRefBranch();
        RefExpectedReason refExpectedReason = MockDataHelper.mockRefExpectedReason();

        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(fiTbl));
        when(branchRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refBranch));
        when(failureClassificationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refFailureClassification));
        when(failureClassificationRepository.findById(any(Long.class))).thenReturn(Optional.of(refFailureClassification));
        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refStatus));
        when(statusRepository.findById(any(Long.class))).thenReturn(Optional.of(refStatus));
        //Radius cannot be found
        when(expectedReasonRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refExpectedReason));

        when(failureInformationRepository.save(any(TblFailureInformation.class)))
                .then((Answer<TblFailureInformation>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblFailureInformation) args[0];
                });

        assertThrows(NotFoundException.class, () -> processHelper.updateFailureInfo(fiDto));

    }

    @Test
    void shouldNotUpdateFailureInformation_Exception_ExpectedReasonNotFound() {
        FailureInformationDto fiDto = MockDataHelper.mockFailureInformationDto();
        TblFailureInformation fiTbl = MockDataHelper.mockTblFailureInformation();
        RefFailureClassification refFailureClassification = MockDataHelper.mockRefFailureClassification();
        RefRadius refRadius = MockDataHelper.mockRefRadius();
        RefStatus refStatus = MockDataHelper.mockRefStatusCreated();
        RefBranch refBranch = MockDataHelper.mockRefBranch();
        TblStation tblStation = MockDataHelper.mockTblStation();
        List<TblAddress> addressList = MockDataHelper.mockTblAddressList();

        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(fiTbl));
        when(branchRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refBranch));
        when(failureClassificationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refFailureClassification));
        when(failureClassificationRepository.findById(any(Long.class))).thenReturn(Optional.of(refFailureClassification));
        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refStatus));
        when(statusRepository.findById(any(Long.class))).thenReturn(Optional.of(refStatus));
        when(radiusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refRadius));
        when(stationRepository.findByStationId(anyString())).thenReturn(Optional.of(tblStation));
        //when(stationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblStation));

        when(addressRepository.findByStationId(anyString())).thenReturn(addressList);

        //ExpectedReason cannot be found
        when(failureInformationRepository.save(any(TblFailureInformation.class)))
                .then((Answer<TblFailureInformation>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblFailureInformation) args[0];
                });

        Throwable exception = assertThrows(NotFoundException.class, () -> processHelper.updateFailureInfo(fiDto));
        assertEquals("expected.reason.uuid.not.existing", exception.getMessage());
    }

    @Test
    void shouldStoreNewFailureInformation() {
        TblFailureInformation fiTbl = MockDataHelper.mockTblFailureInformation();
        FailureInformationDto fiDto = MockDataHelper.mockFailureInformationDto();
        List<UUID> stationList = new ArrayList<>();
        stationList.add(UUID.fromString("8a5fb5ca-d26d-11ea-87d0-0242ac130003"));
        stationList.add(UUID.fromString("8a5fb2aa-d26d-11ea-87d0-0242ac130003"));
        stationList.add(UUID.fromString("a1375ec8-d26e-11ea-87d0-0242ac130003"));
        fiDto.setStationIds(stationList);
        RefFailureClassification refFailureClassification = MockDataHelper.mockRefFailureClassification();
        RefStatus refStatus = MockDataHelper.mockRefStatusCreated();
        RefBranch refBranch = MockDataHelper.mockRefBranch();
        RefRadius refRadius = MockDataHelper.mockRefRadius();
        RefExpectedReason refExpectedReason = MockDataHelper.mockRefExpectedReason();
        TblStation tblStation = MockDataHelper.mockTblStation();
        TblStation tblStation2 = MockDataHelper.mockTblStation2();
        TblStation tblStation3 = MockDataHelper.mockTblStation3();
        TblDistributionGroup tblDistributionGroup = MockDataHelper.mockTblDistributionGroup();
        List<TblFailureInformationPublicationChannel> publicationChannelList = MockDataHelper.mockTblFailureInformationPublicationChannelList();

        when(statusRepository.findById(anyLong())).thenReturn(Optional.of(refStatus));
        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(fiTbl), Optional.of(fiTbl));
        when(branchRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refBranch));
        when(failureClassificationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refFailureClassification));
        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refStatus));
        when(radiusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refRadius));
        when(expectedReasonRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refExpectedReason));
        when(stationRepository.findByUuid(eq(UUID.fromString("8a5fb2aa-d26d-11ea-87d0-0242ac130003")))).thenReturn(Optional.of(tblStation));
        when(stationRepository.findByUuid(eq(UUID.fromString("8a5fb5ca-d26d-11ea-87d0-0242ac130003")))).thenReturn(Optional.of(tblStation2));
        when(stationRepository.findByUuid(eq(UUID.fromString("a1375ec8-d26e-11ea-87d0-0242ac130003")))).thenReturn(Optional.of(tblStation3));
        when(distributionGroupRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblDistributionGroup));
        when(failureInformationPublicationChannelRepository.findByTblFailureInformation(any(TblFailureInformation.class))).thenReturn(publicationChannelList);

        fiDto.setUuid(null);

        when(failureInformationRepository.save(any(TblFailureInformation.class)))
                .then((Answer<TblFailureInformation>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblFailureInformation) args[0];
                });

        FailureInformationDto savedDto = failureInformationService.storeFailureInfo(fiDto, GfiProcessState.NEW);

        assertNotNull(savedDto.getUuid());
        assertEquals(1L, savedDto.getVersionNumber());
        assertEquals(fiDto.getResponsibility(), savedDto.getResponsibility());
        assertEquals(fiDto.getVoltageLevel(), savedDto.getVoltageLevel());
        assertEquals(fiDto.getPressureLevel(), savedDto.getPressureLevel());
        assertEquals(fiDto.getFailureBegin(), savedDto.getFailureBegin());
        assertEquals(fiDto.getFailureEndPlanned(), savedDto.getFailureEndPlanned());
        assertEquals(fiDto.getFailureEndResupplied(), savedDto.getFailureEndResupplied());
        assertEquals(fiDto.getDescription(), savedDto.getDescription());
        assertEquals(fiDto.getPostcode(), savedDto.getPostcode());
        assertEquals(fiDto.getCity(), savedDto.getCity());
        assertEquals(fiDto.getDistrict(), savedDto.getDistrict());
        assertEquals(fiDto.getStreet(), savedDto.getStreet());
        assertEquals(fiDto.getHousenumber(), savedDto.getHousenumber());
        assertEquals(fiDto.getStationDescription(), savedDto.getStationDescription());
        assertEquals(fiDto.getStationCoords(), savedDto.getStationCoords());
        assertEquals(fiTbl.getLongitude(), savedDto.getLongitude());
        assertEquals(fiTbl.getLatitude(), savedDto.getLatitude());
        assertEquals(refFailureClassification.getUuid(), savedDto.getFailureClassificationId());
        assertEquals(refFailureClassification.getClassification(), savedDto.getFailureClassification());
        assertEquals(refStatus.getStatus(), savedDto.getStatusIntern());
        //assertEquals(refStatus.getStatus(), savedDto.getStatusExtern());
        assertEquals(refBranch.getUuid(), savedDto.getBranchId());
        assertEquals(refBranch.getName(), savedDto.getBranch());
        assertEquals(refBranch.getColorCode(), savedDto.getBranchColorCode());
        assertEquals(refRadius.getUuid(), savedDto.getRadiusId());
        assertEquals(refRadius.getRadius(), savedDto.getRadius());
        assertEquals(refExpectedReason.getUuid(), savedDto.getExpectedReasonId());
        assertEquals(refExpectedReason.getText(), savedDto.getExpectedReasonText());
    }

    @Test
    void shouldInsertNewFailureInformation() {
        FailureInformationDto fiDto = MockDataHelper.mockFailureInformationDto();
        List<UUID> stationList = new ArrayList<>();
        stationList.add(UUID.fromString("8a5fb5ca-d26d-11ea-87d0-0242ac130003"));
        stationList.add(UUID.fromString("8a5fb2aa-d26d-11ea-87d0-0242ac130003"));
        stationList.add(UUID.fromString("a1375ec8-d26e-11ea-87d0-0242ac130003"));
        fiDto.setStationIds(stationList);
        TblFailureInformation fiTbl = MockDataHelper.mockTblFailureInformation();
        RefFailureClassification refFailureClassification = MockDataHelper.mockRefFailureClassification();
        RefStatus refStatus = MockDataHelper.mockRefStatusCreated();
        RefBranch refBranch = MockDataHelper.mockRefBranch();
        RefRadius refRadius = MockDataHelper.mockRefRadius();
        RefExpectedReason refExpectedReason = MockDataHelper.mockRefExpectedReason();
        TblStation tblStation = MockDataHelper.mockTblStation();
        TblStation tblStation2 = MockDataHelper.mockTblStation2();
        TblStation tblStation3 = MockDataHelper.mockTblStation3();
        TblDistributionGroup tblDistributionGroup = MockDataHelper.mockTblDistributionGroup();
        List<TblFailureInformationPublicationChannel> publicationChannelList = MockDataHelper.mockTblFailureInformationPublicationChannelList();

        when(statusRepository.findById(anyLong())).thenReturn(Optional.of(refStatus));
        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(fiTbl), Optional.of(fiTbl));
        when(branchRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refBranch));
        when(failureClassificationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refFailureClassification));
        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refStatus));
        when(radiusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refRadius));
        when(expectedReasonRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refExpectedReason));
        when(stationRepository.findByUuid(eq(UUID.fromString("8a5fb2aa-d26d-11ea-87d0-0242ac130003")))).thenReturn(Optional.of(tblStation));
        when(stationRepository.findByUuid(eq(UUID.fromString("8a5fb5ca-d26d-11ea-87d0-0242ac130003")))).thenReturn(Optional.of(tblStation2));
        when(stationRepository.findByUuid(eq(UUID.fromString("a1375ec8-d26e-11ea-87d0-0242ac130003")))).thenReturn(Optional.of(tblStation3));
        when(distributionGroupRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblDistributionGroup));
        when(failureInformationPublicationChannelRepository.findByTblFailureInformation(any(TblFailureInformation.class))).thenReturn(publicationChannelList);

        fiDto.setUuid(null);

        when(failureInformationRepository.save(any(TblFailureInformation.class)))
                .then((Answer<TblFailureInformation>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblFailureInformation) args[0];
                });

        FailureInformationDto savedDto = failureInformationService.insertFailureInfo(fiDto, GfiProcessState.NEW);

        assertNotNull(savedDto.getUuid());
        assertEquals(1L, savedDto.getVersionNumber());
        assertEquals(fiDto.getResponsibility(), savedDto.getResponsibility());
        assertEquals(fiDto.getVoltageLevel(), savedDto.getVoltageLevel());
        assertEquals(fiDto.getPressureLevel(), savedDto.getPressureLevel());
        assertEquals(fiDto.getFailureBegin(), savedDto.getFailureBegin());
        assertEquals(fiDto.getFailureEndPlanned(), savedDto.getFailureEndPlanned());
        assertEquals(fiDto.getFailureEndResupplied(), savedDto.getFailureEndResupplied());
        assertEquals(fiDto.getDescription(), savedDto.getDescription());
        assertEquals(fiDto.getPostcode(), savedDto.getPostcode());
        assertEquals(fiDto.getCity(), savedDto.getCity());
        assertEquals(fiDto.getDistrict(), savedDto.getDistrict());
        assertEquals(fiDto.getStreet(), savedDto.getStreet());
        assertEquals(fiDto.getHousenumber(), savedDto.getHousenumber());
        assertEquals(fiDto.getStationDescription(), savedDto.getStationDescription());
        assertEquals(fiDto.getStationCoords(), savedDto.getStationCoords());
        assertEquals(fiTbl.getLongitude(), savedDto.getLongitude());
        assertEquals(fiTbl.getLatitude(), savedDto.getLatitude());
        assertEquals(refFailureClassification.getUuid(), savedDto.getFailureClassificationId());
        assertEquals(refFailureClassification.getClassification(), savedDto.getFailureClassification());
        assertEquals(refStatus.getStatus(), savedDto.getStatusIntern());
        assertEquals(refBranch.getUuid(), savedDto.getBranchId());
        assertEquals(refBranch.getName(), savedDto.getBranch());
        assertEquals(refBranch.getColorCode(), savedDto.getBranchColorCode());
        assertEquals(refRadius.getUuid(), savedDto.getRadiusId());
        assertEquals(refRadius.getRadius(), savedDto.getRadius());
        assertEquals(refExpectedReason.getUuid(), savedDto.getExpectedReasonId());
        assertEquals(refExpectedReason.getText(), savedDto.getExpectedReasonText());
    }


    @Test
    void shouldNotCondenseFailureInformations_DifferentBranches() {

        List uuidList = MockDataHelper.mockUuidList();
        List<TblFailureInformation> listFailureInfos = MockDataHelper.mockTblFailureInformationList();
        RefBranch branch = MockDataHelper.mockRefBranch2();
        listFailureInfos.get(0).setRefBranch(branch);

        when(failureInformationRepository.findByUuidIn(any(List.class))).thenReturn(listFailureInfos);

        assertThrows(OperationDeniedException.class, () -> failureInformationService.condenseFailureInfos(uuidList, Optional.empty()));
    }

    @Test
    void shouldCondenseFailureInformations_equalFailureInfos_completeAddress() {

        List uuidList = MockDataHelper.mockUuidList();

        TblFailureInformation tblFailureInformation1 = MockDataHelper.mockTblFailureInformation();
        TblFailureInformation tblFailureInformation2 = MockDataHelper.mockTblFailureInformation();
        TblFailureInformation tblFailureInformation3 = MockDataHelper.mockTblFailureInformation();

        List<TblFailureInformation> listFailureInfos = new ArrayList();
        listFailureInfos.add(tblFailureInformation1);
        listFailureInfos.add(tblFailureInformation2);
        listFailureInfos.add(tblFailureInformation3);

        RefStatus refStatus = MockDataHelper.mockRefStatusCreated();

        when(failureInformationRepository.findByUuidIn(any(List.class))).thenReturn(listFailureInfos);
        when(statusRepository.findById(anyLong())).thenReturn(Optional.of(refStatus));
        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refStatus));

        when(failureInformationRepository.save(any(TblFailureInformation.class)))
                .then((Answer<TblFailureInformation>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblFailureInformation) args[0];
                });

        FailureInformationDto savedCondensedFailureInfoDto = failureInformationService.condenseFailureInfos(uuidList, Optional.empty());

        assertNotNull(savedCondensedFailureInfoDto.getUuid());
        assertEquals(1L, savedCondensedFailureInfoDto.getVersionNumber());
        //TODO: status (2x) und radius überprüfen
        assertEquals(tblFailureInformation1.getStreet(), savedCondensedFailureInfoDto.getStreet());
        assertEquals(tblFailureInformation1.getDistrict(), savedCondensedFailureInfoDto.getDistrict());
        assertEquals(tblFailureInformation1.getCity(), savedCondensedFailureInfoDto.getCity());
        assertEquals(tblFailureInformation1.getPostcode(), savedCondensedFailureInfoDto.getPostcode());
        assertEquals(tblFailureInformation1.getHousenumber(), savedCondensedFailureInfoDto.getHousenumber());
        assertEquals(tblFailureInformation1.getLatitude(), savedCondensedFailureInfoDto.getLatitude());
        assertEquals(tblFailureInformation1.getLongitude(), savedCondensedFailureInfoDto.getLongitude());
        assertEquals(tblFailureInformation1.getRefExpectedReason().getUuid(), savedCondensedFailureInfoDto.getExpectedReasonId());
        assertEquals(tblFailureInformation1.getFailureEndPlanned(), savedCondensedFailureInfoDto.getFailureEndPlanned());
        assertEquals(tblFailureInformation1.getFailureBegin(), savedCondensedFailureInfoDto.getFailureBegin());
        assertEquals(true, savedCondensedFailureInfoDto.getCondensed());
        assertEquals(null, savedCondensedFailureInfoDto.getFailureInformationCondensedId());

    }

    @Test
    void shouldCondenseFailureInformations_setAddressNull_equalFailureInfos_notCompleteAddress() {

        List uuidList = MockDataHelper.mockUuidList();

        TblFailureInformation tblFailureInformation1 = MockDataHelper.mockTblFailureInformationWithoutAddress();
        TblFailureInformation tblFailureInformation2 = MockDataHelper.mockTblFailureInformationWithoutAddress();
        TblFailureInformation tblFailureInformation3 = MockDataHelper.mockTblFailureInformationWithoutAddress();

        List<TblFailureInformation> listFailureInfos = new ArrayList();
        listFailureInfos.add(tblFailureInformation1);
        listFailureInfos.add(tblFailureInformation2);
        listFailureInfos.add(tblFailureInformation3);

        RefStatus refStatus = MockDataHelper.mockRefStatusCreated();

        when(failureInformationRepository.findByUuidIn(any(List.class))).thenReturn(listFailureInfos);
        when(statusRepository.findById(anyLong())).thenReturn(Optional.of(refStatus));
        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refStatus));

        when(failureInformationRepository.save(any(TblFailureInformation.class)))
                .then((Answer<TblFailureInformation>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblFailureInformation) args[0];
                });

        FailureInformationDto savedCondensedFailureInfoDto = failureInformationService.condenseFailureInfos(uuidList, Optional.empty());

        assertNotNull(savedCondensedFailureInfoDto.getUuid());
        assertEquals(1L, savedCondensedFailureInfoDto.getVersionNumber());
        //TODO: status (2x) und radius überprüfen
        assertNull(savedCondensedFailureInfoDto.getStreet());
        assertNull(savedCondensedFailureInfoDto.getDistrict());
        assertNull(savedCondensedFailureInfoDto.getCity());
        assertNull(savedCondensedFailureInfoDto.getPostcode());
        assertNull(savedCondensedFailureInfoDto.getHousenumber());
        assertNull(savedCondensedFailureInfoDto.getFreetextCity());
        assertNull(savedCondensedFailureInfoDto.getFreetextDistrict());
        assertNull(savedCondensedFailureInfoDto.getFreetextPostcode());
        assertNotNull(savedCondensedFailureInfoDto.getLatitude());
        assertNotNull(savedCondensedFailureInfoDto.getLongitude());
        assertEquals(tblFailureInformation1.getRefExpectedReason().getUuid(), savedCondensedFailureInfoDto.getExpectedReasonId());
        assertEquals(tblFailureInformation1.getFailureEndPlanned(), savedCondensedFailureInfoDto.getFailureEndPlanned());
        assertEquals(tblFailureInformation1.getFailureBegin(), savedCondensedFailureInfoDto.getFailureBegin());
        assertEquals(true, savedCondensedFailureInfoDto.getCondensed());
        assertNull(savedCondensedFailureInfoDto.getFailureInformationCondensedId());
    }

    @Test
    void shouldCondenseFailureInformations_setAddressNull_differentFailureInfos_differentPostcodeStreetHousenumber() {
        List<UUID> uuidList = MockDataHelper.mockUuidList();

        //different failureInfos and different postcode, street and house number
        TblFailureInformation tblFailureInformation1 = MockDataHelper.mockTblFailureInformation();
        TblFailureInformation tblFailureInformation3 = MockDataHelper.mockTblFailureInformation2();

        TblStation tblStation = MockDataHelper.mockTblStation();
        List<TblAddress> addressList = MockDataHelper.mockTblAddressList();

        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            java.util.Date date1 = dateformat.parse("01/01/2020");
            java.util.Date date2 = dateformat.parse("24/12/2024");

            // set first startdate and last enddate
            tblFailureInformation1.setFailureBegin(date1);
            tblFailureInformation3.setFailureEndPlanned(date2);
        }catch(Exception e){
            log.error("Error in FailureInformationServiceTest: SimpleDateFormat.parse");
        }


        List<TblFailureInformation> listFailureInfos = new ArrayList();
        listFailureInfos.add(tblFailureInformation1);
        listFailureInfos.add(tblFailureInformation3);

        //different status
        RefStatus refStatus = MockDataHelper.mockRefStatusCreated();
        RefStatus refStatus2 = MockDataHelper.mockRefStatus2();

        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refStatus));
        when(failureInformationRepository.findByUuidIn(any(List.class))).thenReturn(listFailureInfos);
        when(statusRepository.findById(anyLong())).thenReturn(Optional.of(refStatus)).thenReturn(Optional.of(refStatus2));
        when(failureInformationRepository.save(any(TblFailureInformation.class)))
                .then((Answer<TblFailureInformation>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblFailureInformation) args[0];
                });
        when(stationRepository.findByStationId(anyString())).thenReturn(Optional.of(tblStation));
        when(addressRepository.findByStationId(anyString())).thenReturn(addressList);

        FailureInformationDto savedCondensedFailureInfoDto = failureInformationService.condenseFailureInfos(uuidList, Optional.empty());

        assertNotNull(savedCondensedFailureInfoDto.getUuid());
        assertEquals(1L, savedCondensedFailureInfoDto.getVersionNumber());
        //TODO: status (2x) und radius überprüfen
        assertNull(savedCondensedFailureInfoDto.getStreet());
        assertNull(savedCondensedFailureInfoDto.getDistrict());
        assertNull(savedCondensedFailureInfoDto.getCity());
        assertNull(savedCondensedFailureInfoDto.getPostcode());
        assertNull(savedCondensedFailureInfoDto.getFreetextCity());
        assertNull(savedCondensedFailureInfoDto.getFreetextDistrict());
        assertNull(savedCondensedFailureInfoDto.getFreetextPostcode());
        assertNull(savedCondensedFailureInfoDto.getLatitude());
        assertNull(savedCondensedFailureInfoDto.getLongitude());
        assertEquals(tblFailureInformation1.getFailureBegin(), savedCondensedFailureInfoDto.getFailureBegin());
        assertEquals(tblFailureInformation3.getFailureEndPlanned(), savedCondensedFailureInfoDto.getFailureEndPlanned());
        assertEquals(true, savedCondensedFailureInfoDto.getCondensed());
        assertEquals(null, savedCondensedFailureInfoDto.getFailureInformationCondensedId());

    }

    @Test
    void shouldCondenseFailureInformations_setAddressRight_differentFailureInfos_samePostcodeStreetHousenumber() {

        List uuidList = MockDataHelper.mockUuidList();

        //different failureInfos but same postcode, street and house number
        TblFailureInformation tblFailureInformation1 = MockDataHelper.mockTblFailureInformation();
        TblFailureInformation tblFailureInformation3 = MockDataHelper.mockTblFailureInformation3();

        TblStation tblStation = MockDataHelper.mockTblStation();
        List<TblAddress> addressList = MockDataHelper.mockTblAddressList();

        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            java.util.Date date1 = dateformat.parse("01/01/2020");
            java.util.Date date2 = dateformat.parse("24/12/2024");

            // set first startdate and last enddate
            tblFailureInformation1.setFailureBegin(date1);
            tblFailureInformation3.setFailureEndPlanned(date2);
        }catch(Exception e){
            log.error("Error in FailureInformationServiceTest: SimpleDateFormat.parse");
        }


        List<TblFailureInformation> listFailureInfos = new ArrayList();
        listFailureInfos.add(tblFailureInformation1);
        listFailureInfos.add(tblFailureInformation3);

        //different status
        RefStatus refStatus = MockDataHelper.mockRefStatusCreated();
        RefStatus refStatus2 = MockDataHelper.mockRefStatus2();

        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refStatus));
        when(failureInformationRepository.findByUuidIn(any(List.class))).thenReturn(listFailureInfos);
        when(statusRepository.findById(anyLong())).thenReturn(Optional.of(refStatus)).thenReturn(Optional.of(refStatus2));
        when(failureInformationRepository.save(any(TblFailureInformation.class)))
                .then((Answer<TblFailureInformation>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblFailureInformation) args[0];
                });
        when(stationRepository.findByStationId(anyString())).thenReturn(Optional.of(tblStation));
        when(addressRepository.findByStationId(anyString())).thenReturn(addressList);

        FailureInformationDto savedCondensedFailureInfoDto = failureInformationService.condenseFailureInfos(uuidList, Optional.empty());

        assertNotNull(savedCondensedFailureInfoDto.getUuid());
        assertEquals(1L, savedCondensedFailureInfoDto.getVersionNumber());
        //TODO: status (2x) und radius überprüfen
        assertEquals(tblFailureInformation1.getStreet(), savedCondensedFailureInfoDto.getStreet());
        assertEquals(tblFailureInformation1.getDistrict(), savedCondensedFailureInfoDto.getDistrict());
        assertEquals(tblFailureInformation1.getCity(), savedCondensedFailureInfoDto.getCity());
        assertEquals(tblFailureInformation1.getPostcode(), savedCondensedFailureInfoDto.getPostcode());
        assertEquals(tblFailureInformation1.getHousenumber(), savedCondensedFailureInfoDto.getHousenumber());
        assertEquals(tblFailureInformation1.getLatitude(), savedCondensedFailureInfoDto.getLatitude());
        assertEquals(tblFailureInformation1.getLongitude(), savedCondensedFailureInfoDto.getLongitude());
        assertEquals(tblFailureInformation1.getFailureBegin(), savedCondensedFailureInfoDto.getFailureBegin());
        assertEquals(tblFailureInformation3.getFailureEndPlanned(), savedCondensedFailureInfoDto.getFailureEndPlanned());
        assertEquals(true, savedCondensedFailureInfoDto.getCondensed());
        assertEquals(null, savedCondensedFailureInfoDto.getFailureInformationCondensedId());

    }

    @Test
    void shouldNotCondenseFailureInformations_oneSelectedInfoIsAlreadySubordinated() {

        List uuidList = MockDataHelper.mockUuidList();

        //different failureInfos
        TblFailureInformation tblFailureInformation1 = MockDataHelper.mockTblFailureInformation();
        TblFailureInformation tblFailureInformation2 = MockDataHelper.mockTblFailureInformation();
        TblFailureInformation tblFailureInformation3 = MockDataHelper.mockTblFailureInformation2();

        //one FailureInformation is a subordinated part of a condensed FailureInformation
        tblFailureInformation1.setTblFailureInformationCondensed(MockDataHelper.mockTblFailureInformation2());

        TblStation tblStation = MockDataHelper.mockTblStation();
        List<TblAddress> addressList = MockDataHelper.mockTblAddressList();

        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            java.util.Date date1 = dateformat.parse("01/01/2020");
            java.util.Date date2 = dateformat.parse("24/12/2024");

            // set first startdate and last enddate
            tblFailureInformation1.setFailureBegin(date1);
            tblFailureInformation3.setFailureEndPlanned(date2);
        }catch(Exception e){
            log.error("Error in FailureInformationServiceTest: SimpleDateFormat.parse");
        }

        List<TblFailureInformation> listFailureInfos = new ArrayList();
        listFailureInfos.add(tblFailureInformation1);
        listFailureInfos.add(tblFailureInformation2);
        listFailureInfos.add(tblFailureInformation3);

        //different status
        RefStatus refStatus = MockDataHelper.mockRefStatusCreated();
        RefStatus refStatus2 = MockDataHelper.mockRefStatus2();

        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refStatus));
        when(failureInformationRepository.findByUuidIn(any(List.class))).thenReturn(listFailureInfos);
        when(statusRepository.findById(anyLong())).thenReturn(Optional.of(refStatus)).thenReturn(Optional.of(refStatus2));
        when(failureInformationRepository.save(any(TblFailureInformation.class)))
                .then((Answer<TblFailureInformation>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblFailureInformation) args[0];
                });
        when(stationRepository.findByStationId(anyString())).thenReturn(Optional.of(tblStation));
        when(addressRepository.findByStationId(anyString())).thenReturn(addressList);

        assertThrows (BadRequestException.class, () -> failureInformationService.condenseFailureInfos(uuidList, Optional.empty()));

    }

    @Test
    void shouldReturnSubordinateFailureInfos(){

        RefStatus refStatus = MockDataHelper.mockRefStatusCreated();
        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refStatus));

        List<TblFailureInformation> fiMockList = MockDataHelper.mockTblFailureInformationList();
        when(failureInformationRepository.findByFailureInformationCondensedUuid(any(UUID.class))).thenReturn(fiMockList);

        List<FailureInformationDto> fiDtoList = failureInformationService.findFailureInformationsByCondensedUuid(UUID.randomUUID());

        assertEquals( fiDtoList.size(), fiMockList.size() );
        assertEquals( fiDtoList.get(0).getResponsibility(), fiMockList .get(0).getResponsibility());

    }

    @Test
    void shouldUpdateSubordinateFailureInfos(){

        TblFailureInformation fi = MockDataHelper.mockTblFailureInformation();
        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(fi));

        List<TblFailureInformation> fiMockList = MockDataHelper.mockTblFailureInformationList();
        when(failureInformationRepository.findByFailureInformationCondensedUuid(any(UUID.class))).thenReturn(fiMockList);

        List<TblFailureInformation> subordinatedFiList = MockDataHelper.mockTblFailureInformationList();
        when(failureInformationRepository.findByUuidIn(anyList())).thenReturn(subordinatedFiList);

        when(statusRepository.findById(anyLong())).thenReturn(Optional.of(MockDataHelper.mockRefStatusCreated()));
        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(MockDataHelper.mockRefStatusCreated()));

        when(failureInformationRepository.save(any(TblFailureInformation.class)))
                .then((Answer<TblFailureInformation>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblFailureInformation) args[0];
                });

        List<UUID> subordinatedFiUuidList = MockDataHelper.mockUuidList();
        FailureInformationDto condensedFi = failureInformationService.updateSubordinatedFailureInfos(UUID.randomUUID(), subordinatedFiUuidList);

        assertEquals( condensedFi.getCondensedCount(), subordinatedFiList.size() );
    }

    @Test
    void shouldNotUpdateSubordinateFailureInfos_emptyUuidList(){

        TblFailureInformation fi = MockDataHelper.mockTblFailureInformation();
        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(fi));

        List<TblFailureInformation> fiMockList = MockDataHelper.mockTblFailureInformationList();
        when(failureInformationRepository.findByFailureInformationCondensedUuid(any(UUID.class))).thenReturn(fiMockList);

        List<TblFailureInformation> subordinatedFiList = MockDataHelper.mockTblFailureInformationList();
        when(failureInformationRepository.findByUuidIn(anyList())).thenReturn(subordinatedFiList);

        List<UUID> subordinatedFiUuidList = new ArrayList<UUID>();

        assertThrows(BadRequestException.class, () -> failureInformationService.updateSubordinatedFailureInfos(UUID.randomUUID(), subordinatedFiUuidList));
    }

    @Test
    void shouldNotUpdateSubordinateFailureInfos_containsAlienSubordinatedFis(){

        List<TblFailureInformation> subordinatedFiList = MockDataHelper.mockTblFailureInformationList();
        TblFailureInformation tblFailureInformation =  MockDataHelper.mockTblFailureInformation();
        subordinatedFiList.get(0).setTblFailureInformationCondensed(tblFailureInformation);

        when(failureInformationRepository.findByUuidIn(anyList())).thenReturn(subordinatedFiList);

        List<UUID> subordinatedFiUuidList = MockDataHelper.mockUuidList();
        assertThrows(BadRequestException.class, () -> failureInformationService.updateSubordinatedFailureInfos(UUID.randomUUID(), subordinatedFiUuidList));
    }

    @Test
    void shouldInsertPublicationChannelForFailureInfo(){

        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();
        TblFailureInformationPublicationChannel tblChannel = MockDataHelper.mockTblFailureInformationPublicationChannel2();

        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));
        when(failureInformationPublicationChannelRepository.countByTblFailureInformationAndPublicationChannel(any(TblFailureInformation.class), anyString())).thenReturn(0);
        when(failureInformationPublicationChannelRepository.save(any(TblFailureInformationPublicationChannel.class))).thenReturn(tblChannel);

        FailureInformationPublicationChannelDto channelDto = failureInformationService.insertPublicationChannelForFailureInfo(tblFailureInformation.getUuid(), tblChannel.getPublicationChannel() , false );

        assertEquals(tblChannel.getTblFailureInformation().getUuid(), channelDto.getFailureInformationId());
        assertEquals(tblChannel.getPublicationChannel(), channelDto.getPublicationChannel());
    }

    @Test
    void shouldNotInsertPublicationChannelForFailureInfo_channelAlreadyExisting(){

        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();
        TblFailureInformationPublicationChannel tblChannel = MockDataHelper.mockTblFailureInformationPublicationChannel2();

        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));
        when(failureInformationPublicationChannelRepository.countByTblFailureInformationAndPublicationChannel(any(TblFailureInformation.class), anyString())).thenReturn(1);
        when(failureInformationPublicationChannelRepository.save(any(TblFailureInformationPublicationChannel.class))).thenReturn(tblChannel);

        assertThrows(BadRequestException.class, () -> failureInformationService.insertPublicationChannelForFailureInfo(tblFailureInformation.getUuid(), tblChannel.getPublicationChannel(), false));
    }

    @Test
    void shouldDeletePublicationChannelForFailureInfo(){

        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();
        TblFailureInformationPublicationChannel tblChannel = MockDataHelper.mockTblFailureInformationPublicationChannel2();

        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));
        when(failureInformationPublicationChannelRepository.findByTblFailureInformationAndPublicationChannel(any(TblFailureInformation.class), anyString())).thenReturn(Optional.of(tblChannel));

        Mockito.doNothing().when(failureInformationPublicationChannelRepository).delete( any(TblFailureInformationPublicationChannel.class));
        failureInformationService.deletePublicationChannelForFailureInfo(tblFailureInformation.getUuid(), tblChannel.getPublicationChannel());

        Mockito.verify(failureInformationPublicationChannelRepository, times(1)).delete( tblChannel );
    }

    @Test
    void shouldUpdateAndPublish() {

        RefStatus statusQualified =  MockDataHelper.mockRefStatusQUALIFIED();
        FailureInformationDto dto = MockDataHelper.mockFailureInformationDto();
        List<UUID> stationList = new ArrayList<>();
        stationList.add(UUID.fromString("8a5fb5ca-d26d-11ea-87d0-0242ac130003"));
        stationList.add(UUID.fromString("8a5fb2aa-d26d-11ea-87d0-0242ac130003"));
        stationList.add(UUID.fromString("a1375ec8-d26e-11ea-87d0-0242ac130003"));
        dto.setStationIds(stationList);
        dto.setVoltageLevel(Constants.VOLTAGE_LEVEL_HS);
        dto.setStatusExtern(null);
        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();
        tblFailureInformation.setVoltageLevel(Constants.VOLTAGE_LEVEL_HS);
        List<TblFailureInformationPublicationChannel> channelList = MockDataHelper.mockTblFailureInformationPublicationChannelList();
        RefBranch refBranch = MockDataHelper.mockRefBranch();
        RefFailureClassification refFailureClassification = MockDataHelper.mockRefFailureClassification();
        RefRadius refRadius = MockDataHelper.mockRefRadius();
        RefExpectedReason refExpectedReason = MockDataHelper.mockRefExpectedReason();
        TblDistributionGroup tblDistributionGroup = MockDataHelper.mockTblDistributionGroup();
        TblStation tblStation = MockDataHelper.mockTblStation();
        TblStation tblStation2 = MockDataHelper.mockTblStation2();
        TblStation tblStation3 = MockDataHelper.mockTblStation3();
        List<TblFailureInformationPublicationChannel> pubChannelList = MockDataHelper.mockTblFailureInformationPublicationChannelList();

        dto.setStatusInternId(statusQualified.getUuid());
        dto.setStatusIntern(statusQualified.getStatus());

        when (statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(statusQualified));
        when (statusRepository.findById(anyLong())).thenReturn(Optional.of(statusQualified));
        when(failureInformationRepository.findByUuid( any(UUID.class) )).thenReturn(Optional.of(tblFailureInformation));
        when(failureInformationPublicationChannelRepository.findByTblFailureInformation(any(TblFailureInformation.class))).thenReturn(channelList);
        when(branchRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refBranch));
        when(failureClassificationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refFailureClassification));
        when(radiusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refRadius));
        when(expectedReasonRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refExpectedReason));
        when(distributionGroupRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblDistributionGroup));
        when(stationRepository.findByUuid(eq(UUID.fromString("8a5fb2aa-d26d-11ea-87d0-0242ac130003")))).thenReturn(Optional.of(tblStation));
        when(stationRepository.findByUuid(eq(UUID.fromString("8a5fb5ca-d26d-11ea-87d0-0242ac130003")))).thenReturn(Optional.of(tblStation2));
        when(stationRepository.findByUuid(eq(UUID.fromString("a1375ec8-d26e-11ea-87d0-0242ac130003")))).thenReturn(Optional.of(tblStation3));
        when(failureInformationPublicationChannelRepository.findByTblFailureInformation(any(TblFailureInformation.class))).thenReturn(pubChannelList);
        when(failureInformationRepository.save(any(TblFailureInformation.class))).thenReturn(tblFailureInformation);

        FailureInformationDto updatedDto = processHelper.updateAndPublish(dto);

        assertEquals(Constants.PUB_STATUS_VEROEFFENTLICHT, updatedDto.getPublicationStatus());
    }

    @Test
    void shouldNotUpdateAndPublish_wrongStatus() {

        RefStatus statusCreated =  MockDataHelper.mockRefStatusCreated();
        FailureInformationDto dto = MockDataHelper.mockFailureInformationDto();
        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();

        dto.setStatusInternId(statusCreated.getUuid());
        dto.setStatusIntern(statusCreated.getStatus());

        when (statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(statusCreated));
        when(failureInformationRepository.findByUuid( any(UUID.class) )).thenReturn(Optional.of(tblFailureInformation));

        assertThrows(BadRequestException.class, () -> processHelper.updateAndPublish(dto));
    }

    @Test
    void shouldNotUpdateAndPublish_noChannel() {

        RefStatus statusCreated =  MockDataHelper.mockRefStatusQUALIFIED();
        FailureInformationDto dto = MockDataHelper.mockFailureInformationDto();
        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();
        List channelList = new ArrayList<TblFailureInformationPublicationChannel>();

        dto.setStatusInternId(statusCreated.getUuid());
        dto.setStatusIntern(statusCreated.getStatus());

        when (statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(statusCreated));
        when(failureInformationRepository.findByUuid( any(UUID.class) )).thenReturn(Optional.of(tblFailureInformation));
        when(failureInformationPublicationChannelRepository.findByTblFailureInformation(any(TblFailureInformation.class))).thenReturn(channelList);

        assertThrows(BadRequestException.class, () -> processHelper.updateAndPublish(dto));
    }

    @Test
    void shouldReturnFailureInfoPublicationChannels(){

        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();
        List<TblFailureInformationPublicationChannel> fipChannelList =  MockDataHelper.mockTblFailureInformationPublicationChannelList();

        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));
        when(failureInformationPublicationChannelRepository.findByTblFailureInformation(any(TblFailureInformation.class))).thenReturn(fipChannelList);

        List<FailureInformationPublicationChannelDto> fipChannelDtoList = failureInformationService.getPublicationChannelsForFailureInfo(UUID.randomUUID());

        assertEquals( fipChannelDtoList.size(), fipChannelList.size() );
        assertEquals( fipChannelDtoList.get(0).getPublicationChannel(), fipChannelList .get(0).getPublicationChannel());

    }

    @Test
    void shouldFindByObjectReferenceExternalSystem(){
        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();
        when( failureInformationRepository.findByObjectReferenceExternalSystem( anyString() )).thenReturn(Optional.of(tblFailureInformation));

        FailureInformationDto existingFi = failureInformationService.findByObjectReferenceExternalSystem("test");

        assertEquals(existingFi.getUuid(), tblFailureInformation.getUuid());
    }

    @Test
    void shouldNotFindByObjectReferenceExternalSystem(){
        when( failureInformationRepository.findByObjectReferenceExternalSystem( anyString() )).thenReturn(Optional.empty());

        FailureInformationDto existingFi = failureInformationService.findByObjectReferenceExternalSystem("test");

        assertNull(existingFi);
    }

    @Test
    void shouldDeleteFailureInfo(){

        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();
        RefStatus refStatus = new RefStatus() ;
        refStatus.setId(1L);
        refStatus.setStatus("neu");

        tblFailureInformation.setRefStatusIntern(refStatus);

        when(failureInformationDistributionGroupRepository.findByFailureInformationId(anyLong())).thenReturn(MockDataHelper.mockTblFailureInformationDistributionGroupList());
        Mockito.doNothing().when(failureInformationDistributionGroupRepository).delete(any(TblFailureInformationDistributionGroup.class));

        when(failureInformationPublicationChannelRepository.findByTblFailureInformation(any(TblFailureInformation.class))).thenReturn(MockDataHelper.mockTblFailureInformationPublicationChannelList());
        Mockito.doNothing().when(failureInformationPublicationChannelRepository).delete(any(TblFailureInformationPublicationChannel.class));

        when(failureInformationReminderMailSentRepository.findByTblFailureInformation(any(TblFailureInformation.class))).thenReturn(Optional.of(MockDataHelper.mockTblFailureInformationReminderMailSent()));
        Mockito.doNothing().when(failureInformationReminderMailSentRepository).delete(any(TblFailureInformationReminderMailSent.class));

        when(failureInformationStationRepository.findByFailureInformationId(anyLong())).thenReturn(MockDataHelper.mockTblFailureInformationStationList());
        Mockito.doNothing().when(failureInformationStationRepository).delete(any(TblFailinfoStation.class));

        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));
        Mockito.doNothing().when(failureInformationRepository).delete( any(TblFailureInformation.class));

        failureInformationService.deleteFailureInfo(tblFailureInformation.getUuid());

        Mockito.verify(failureInformationRepository, times(1)).delete( tblFailureInformation );
    }

    @Test
    void shouldNotDeleteFailureInfo_wrongStatus(){

        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();
        RefStatus refStatus = new RefStatus() ;
        refStatus.setId(3L);
        refStatus.setStatus("beendet");

        tblFailureInformation.setRefStatusIntern(refStatus);

        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));
        Mockito.doNothing().when(failureInformationRepository).delete( any(TblFailureInformation.class));

        assertThrows(BadRequestException.class, () -> failureInformationService.deleteFailureInfo(tblFailureInformation.getUuid()));
    }

    @Test
    void shouldGetLastModTimestampCorrectly() {
        List<TblFailureInformation> visibleList = MockDataHelper.mockTblFailureInformationList();
        when( failureInformationRepository.findByTblFailureInformationForDisplay(anyLong(), anyLong(), any( Date.class ), any(Pageable.class)))
                .thenReturn(new PageImpl<>(visibleList));

        long nowInMillis = System.currentTimeMillis();
        visibleList.get(0).setModDate( Date.from(Instant.ofEpochMilli(nowInMillis - 100000L)  ));
        visibleList.get(1).setModDate( Date.from(Instant.ofEpochMilli(nowInMillis + 200000L)  ));
        visibleList.get(2).setModDate( Date.from(Instant.ofEpochMilli(nowInMillis + 100000L)  ));
        visibleList.get(3).setModDate( Date.from(Instant.ofEpochMilli(nowInMillis)  ));

        FailureInformationLastModDto resultDto = failureInformationService.getFailureInformationLastModTimeStamp();
        assertEquals( visibleList.get(1).getModDate(), resultDto.getLastModification());
        assertEquals( visibleList.get(1).getUuid(), resultDto.getUuid() );
     }


    @Test
    void shouldGetLastModTimestampWithException() {
        when( failureInformationRepository.findByTblFailureInformationForDisplay(anyLong(), anyLong(), any( Date.class ), any(Pageable.class)))
                .thenReturn(new PageImpl<>(new ArrayList<>()));

        assertThrows(NotFoundException.class, () -> failureInformationService.getFailureInformationLastModTimeStamp());
    }


}
