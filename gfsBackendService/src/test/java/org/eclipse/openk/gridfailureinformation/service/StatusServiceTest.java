/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.model.RefStatus;
import org.eclipse.openk.gridfailureinformation.repository.StatusRepository;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.StatusDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class StatusServiceTest {
    @Autowired
    private StatusService statusService;

    @Autowired
    private StatusRepository statusRepository;

    @Test
    public void shouldGetStatusProperly() {
        List<RefStatus> mockRefStatusList = MockDataHelper.mockRefStatusList();
        when(statusRepository.findAll()).thenReturn(mockRefStatusList);
        List<StatusDto> listRefStatus = statusService.getStatus();

        assertEquals(listRefStatus.size(), mockRefStatusList.size());
        assertEquals(2, listRefStatus.size());
        assertEquals(listRefStatus.get(1).getUuid(), mockRefStatusList.get(1).getUuid());
    }
}
