/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.bpmn.base;


import org.eclipse.openk.gridfailureinformation.bpmn.base.tasks.BaseTask;

public class BaseTaskImpl extends BaseTask<TestProcessSubject> {
    public boolean leaveStepCalled = false;
    public boolean enterStepCalled = false;

    public BaseTaskImpl( String description ) {
        super(description);
    }

    @Override
    protected void onLeaveStep(TestProcessSubject model) throws ProcessException {
        this.leaveStepCalled = true;
    }

    @Override
    protected void onEnterStep(TestProcessSubject model) throws ProcessException {
        this.enterStepCalled = true;
    }

    @Override
    protected void onRecover(TestProcessSubject model) throws ProcessException {
        this.leaveStep(model);

    }
}
