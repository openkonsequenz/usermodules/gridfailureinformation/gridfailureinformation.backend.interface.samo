/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.bpmn.base.tasks;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessException;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessSubject;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessTask;

import java.util.EnumMap;
import java.util.Map;

@Log4j2
public abstract class DecisionTask<T extends ProcessSubject> extends BaseTask<T> {
    public enum OutputPort {PORT1, PORT2, PORT3, PORT4, PORT5, YES, NO}
    private final Map<OutputPort, ProcessTask> outputMap = new EnumMap<>(OutputPort.class);

    protected DecisionTask(String description) {
        super(description);
    }

    public void connectOutputTo(OutputPort port, ProcessTask step ) {
        outputMap.put(port, step);
    }

    public abstract OutputPort decide( T model ) throws ProcessException;

    @Override
    public void enterStep(ProcessSubject model) throws ProcessException {
        log.debug("Enter: \""+getDescription()+"\"");
        OutputPort port = decide((T)model);
        if( !outputMap.containsKey(port) ){
            throw new ProcessException(getDescription()+": Usage of unconnected output "+port.name());
        }
        else {
            outputMap.get(port).enterStep(model);
        }
    }

    @Override
    protected void onEnterStep(T model) {

    }

    @Override
    protected void onLeaveStep(T model) throws ProcessException {
        throw new ProcessException("onLeaveStep must not be called on a decision task object");
    }

    @Override
    protected void onRecover(T model) throws ProcessException {
        // Entry-point is enter!
        enterStep(model);
    }








}
