package org.eclipse.openk.gridfailureinformation.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
public class VisibilityConfig {
    @Data
    @Configuration
    public static class FieldVisibility {
        @Value("${spring.settings.visibilityConfiguration.fieldVisibility.failureClassification}")
        private String failureClassification;
        @Value("${spring.settings.visibilityConfiguration.fieldVisibility.responsibility}")
        private String responsibility;
        @Value("${spring.settings.visibilityConfiguration.fieldVisibility.description}")
        private String description;
        @Value("${spring.settings.visibilityConfiguration.fieldVisibility.internalRemark}")
        private String internalRemark;
    }
    @Data
    @Configuration
    public static class TableInternColumnVisibility {
        @Value( "${spring.settings.visibilityConfiguration.tableInternColumnVisibility.failureClassification}")
        private String failureClassification;
        @Value( "${spring.settings.visibilityConfiguration.tableInternColumnVisibility.responsibility}")
        private String responsibility;
        @Value( "${spring.settings.visibilityConfiguration.tableInternColumnVisibility.description}")
        private String description;
        @Value( "${spring.settings.visibilityConfiguration.tableInternColumnVisibility.statusIntern}")
        private String statusIntern;
        @Value( "${spring.settings.visibilityConfiguration.tableInternColumnVisibility.statusExtern}")
        private String statusExtern;
        @Value( "${spring.settings.visibilityConfiguration.tableInternColumnVisibility.publicationStatus}")
        private String publicationStatus;
        @Value( "${spring.settings.visibilityConfiguration.tableInternColumnVisibility.branch}")
        private String branch;
        @Value( "${spring.settings.visibilityConfiguration.tableInternColumnVisibility.voltageLevel}")
        private String voltageLevel;
        @Value( "${spring.settings.visibilityConfiguration.tableInternColumnVisibility.pressureLevel}")
        private String pressureLevel;
        @Value( "${spring.settings.visibilityConfiguration.tableInternColumnVisibility.failureBegin}")
        private String failureBegin;
        @Value( "${spring.settings.visibilityConfiguration.tableInternColumnVisibility.failureEndPlanned}")
        private String failureEndPlanned;
        @Value( "${spring.settings.visibilityConfiguration.tableInternColumnVisibility.failureEndResupplied}")
        private String failureEndResupplied;
        @Value( "${spring.settings.visibilityConfiguration.tableInternColumnVisibility.expectedReasonText}")
        private String expectedReasonText;
        @Value( "${spring.settings.visibilityConfiguration.tableInternColumnVisibility.internalRemark}")
        private String internalRemark;
        @Value( "${spring.settings.visibilityConfiguration.tableInternColumnVisibility.postcode}")
        private String postcode;
        @Value( "${spring.settings.visibilityConfiguration.tableInternColumnVisibility.city}")
        private String city;
        @Value( "${spring.settings.visibilityConfiguration.tableInternColumnVisibility.district}")
        private String district;
        @Value( "${spring.settings.visibilityConfiguration.tableInternColumnVisibility.street}")
        private String street;
        @Value( "${spring.settings.visibilityConfiguration.tableInternColumnVisibility.housenumber}")
        private String housenumber;
        @Value( "${spring.settings.visibilityConfiguration.tableInternColumnVisibility.radius}")
        private String radius;
        @Value( "${spring.settings.visibilityConfiguration.tableInternColumnVisibility.stationIds}")
        private String stationIds;
    }

    @Data
    @Configuration
    public static class TableExternColumnVisibility {
        @Value( "${spring.settings.visibilityConfiguration.tableExternColumnVisibility.branch}")
        private String branch;
        @Value( "${spring.settings.visibilityConfiguration.tableExternColumnVisibility.failureBegin}")
        private String failureBegin;
        @Value( "${spring.settings.visibilityConfiguration.tableExternColumnVisibility.failureEndPlanned}")
        private String failureEndPlanned;
        @Value( "${spring.settings.visibilityConfiguration.tableExternColumnVisibility.expectedReasonText}")
        private String expectedReasonText;
        @Value( "${spring.settings.visibilityConfiguration.tableExternColumnVisibility.description}")
        private String description;
        @Value( "${spring.settings.visibilityConfiguration.tableExternColumnVisibility.postcode}")
        private String postcode;
        @Value( "${spring.settings.visibilityConfiguration.tableExternColumnVisibility.city}")
        private String city;
        @Value( "${spring.settings.visibilityConfiguration.tableExternColumnVisibility.district}")
        private String district;
        @Value( "${spring.settings.visibilityConfiguration.tableExternColumnVisibility.street}")
        private String street;
        @Value( "${spring.settings.visibilityConfiguration.tableExternColumnVisibility.failureClassification}")
        private String failureClassification;
    }
    @Data
    @Configuration
    public static class MapExternTooltipVisibility {
        @Value( "${spring.settings.visibilityConfiguration.mapExternTooltipVisibility.failureBegin}")
        private String failureBegin;
        @Value( "${spring.settings.visibilityConfiguration.mapExternTooltipVisibility.failureEndPlanned}")
        private String failureEndPlanned;
        @Value( "${spring.settings.visibilityConfiguration.mapExternTooltipVisibility.expectedReasonText}")
        private String expectedReasonText;
        @Value( "${spring.settings.visibilityConfiguration.mapExternTooltipVisibility.branch}")
        private String branch;
        @Value( "${spring.settings.visibilityConfiguration.mapExternTooltipVisibility.postcode}")
        private String postcode;
        @Value( "${spring.settings.visibilityConfiguration.mapExternTooltipVisibility.city}")
        private String city;
        @Value( "${spring.settings.visibilityConfiguration.mapExternTooltipVisibility.district}")
        private String district;
    }

    private FieldVisibility fieldVisibility;
    private TableInternColumnVisibility tableInternColumnVisibility;
    private TableExternColumnVisibility tableExternColumnVisibility;
    private MapExternTooltipVisibility mapExternTooltipVisibility;

    public VisibilityConfig(
            FieldVisibility fieldVisibility,
            TableInternColumnVisibility tableInternColumnVisibility,
            TableExternColumnVisibility tableExternColumnVisibility,
            MapExternTooltipVisibility mapExternTooltipVisibility
    ) {
        this.fieldVisibility = fieldVisibility;
        this.tableInternColumnVisibility = tableInternColumnVisibility;
        this.tableExternColumnVisibility = tableExternColumnVisibility;
        this.mapExternTooltipVisibility = mapExternTooltipVisibility;
    }
}
