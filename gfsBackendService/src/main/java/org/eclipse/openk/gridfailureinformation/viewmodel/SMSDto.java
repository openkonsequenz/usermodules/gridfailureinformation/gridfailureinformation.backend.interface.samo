package org.eclipse.openk.gridfailureinformation.viewmodel;

import java.util.ArrayList;
import java.util.List;

public class SMSDto {

    private String Apikey = "";
    private List<String> Numbers= new ArrayList<>();
    private String TextBody = "";

    public SMSDto() {}

    public SMSDto(String apikey, List<String> numbers, String textBody) {
        Apikey = apikey;
        Numbers = numbers;
        TextBody = textBody;
    }

    public String getApikey() {
        return Apikey;
    }

    public void setApikey(String apikey) {
        Apikey = apikey;
    }

    public List<String> getNumbers() {
        return Numbers;
    }

    public void setNumbers(List<String> numbers) {
        Numbers = numbers;
    }

    public String getTextBody() {
        return TextBody;
    }

    public void setTextBody(String textBody) {
        TextBody = textBody;
    }
}
