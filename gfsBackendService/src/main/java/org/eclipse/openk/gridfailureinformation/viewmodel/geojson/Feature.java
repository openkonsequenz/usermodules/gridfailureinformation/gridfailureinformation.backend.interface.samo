package org.eclipse.openk.gridfailureinformation.viewmodel.geojson;

import lombok.Data;

@Data
public class Feature {

    private String type;
    private Geometry geometry;

}