/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.enums.OperationType;
import org.eclipse.openk.gridfailureinformation.exceptions.BadRequestException;
import org.eclipse.openk.gridfailureinformation.exceptions.NotFoundException;
import org.eclipse.openk.gridfailureinformation.exceptions.OperationDeniedException;
import org.eclipse.openk.gridfailureinformation.mapper.DistributionGroupMapper;
import org.eclipse.openk.gridfailureinformation.mapper.FailureInformationDistributionGroupMapper;
import org.eclipse.openk.gridfailureinformation.model.TblDistributionGroup;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformationDistributionGroup;
import org.eclipse.openk.gridfailureinformation.repository.DistributionGroupRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationDistributionGroupRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionGroupDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDistributionGroupDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class FailureInformationDistributionGroupService {
    private final DistributionGroupRepository distributionGroupRepository;

    private final DistributionGroupMapper distributionGroupMapper;

    private final FailureInformationRepository failureInformationRepository;

    private final FailureInformationDistributionGroupRepository failureInformationDistributionGroupRepository;

    private final FailureInformationDistributionGroupMapper failureInformationDistributionGroupMapper;

    public FailureInformationDistributionGroupService(DistributionGroupRepository distributionGroupRepository, DistributionGroupMapper distributionGroupMapper, FailureInformationRepository failureInformationRepository, FailureInformationDistributionGroupRepository failureInformationDistributionGroupRepository, FailureInformationDistributionGroupMapper failureInformationDistributionGroupMapper) {
        this.distributionGroupRepository = distributionGroupRepository;
        this.distributionGroupMapper = distributionGroupMapper;
        this.failureInformationRepository = failureInformationRepository;
        this.failureInformationDistributionGroupRepository = failureInformationDistributionGroupRepository;
        this.failureInformationDistributionGroupMapper = failureInformationDistributionGroupMapper;
    }

    public List<DistributionGroupDto> findDistributionGroupsByFailureInfo(UUID failureInfoUuid ) {
        TblFailureInformation existingTblFailureInfo = failureInformationRepository.findByUuid( failureInfoUuid )
                .orElseThrow(NotFoundException::new);
        return failureInformationDistributionGroupRepository.findByFailureInformationId(existingTblFailureInfo.getId())
                .stream()
                .map( TblFailureInformationDistributionGroup::getDistributionGroup )
                .map( distributionGroupMapper::toDistributionGroupDto )
                .collect(Collectors.toList());
    }

    @Transactional
    public FailureInformationDistributionGroupDto insertFailureInfoDistributionGroup(UUID failureInfoUuid, DistributionGroupDto groupDto) {

        TblFailureInformation failureInformation = failureInformationRepository.findByUuid(failureInfoUuid)
                .orElseThrow( () -> new NotFoundException("failure.info.uuid.not.existing"));

        TblDistributionGroup distributionGroup = distributionGroupRepository.findByUuid(groupDto.getUuid())
                .orElseThrow( () -> new NotFoundException("distribution.group.uuid.not.existing"));

        List<TblDistributionGroup> distributionGroups =  failureInformationDistributionGroupRepository.findByFailureInformationId(failureInformation.getId()).stream()
                .map( TblFailureInformationDistributionGroup::getDistributionGroup)
                .toList();

        for (TblDistributionGroup group : distributionGroups) {
            if (group.getUuid().equals(groupDto.getUuid())) {
                throw new OperationDeniedException(OperationType.INSERT, "assignment.already.existing");
            }
        }

        TblFailureInformationDistributionGroup assignmentToSave = new TblFailureInformationDistributionGroup();
        assignmentToSave.setFailureInformation(failureInformation);
        assignmentToSave.setDistributionGroup(distributionGroup);

        TblFailureInformationDistributionGroup savedFailureInfoGroup = failureInformationDistributionGroupRepository.save(assignmentToSave);

        return failureInformationDistributionGroupMapper.toFailureInfoDistributionGroupDto(savedFailureInfoGroup);

    }

    @Transactional
    public void deleteFailureInfoDistributionGroup(UUID failureInfoUuid, UUID groupUuid) {
        TblFailureInformation failureInformation = failureInformationRepository.findByUuid(failureInfoUuid)
                .orElseThrow( () -> new BadRequestException("failure.info.uuid.not.existing"));

        TblDistributionGroup distributionGroup = distributionGroupRepository.findByUuid(groupUuid)
                .orElseThrow( () -> new BadRequestException("distribution.group.uuid.not.existing"));

        TblFailureInformationDistributionGroup tblFailureInformationDistributionGroup
                = failureInformationDistributionGroupRepository.findByFailureInformationIdAndDistributionGroupId(failureInformation.getId(), distributionGroup.getId())
                    .orElseThrow(() -> new BadRequestException("failure.info.distribution.group.not.existing"));

        failureInformationDistributionGroupRepository.delete(tblFailureInformationDistributionGroup);
    }
}
