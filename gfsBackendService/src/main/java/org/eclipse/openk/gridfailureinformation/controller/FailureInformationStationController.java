/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationStationService;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationStationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.StationDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.UUID;

@Log4j2
@RestController
@RequestMapping("/grid-failure-informations")
public class FailureInformationStationController {
    private final FailureInformationStationService failureInformationStationService;

    public FailureInformationStationController(FailureInformationStationService failureInformationStationService) {
        this.failureInformationStationService = failureInformationStationService;
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN","ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @GetMapping("/{failureInfoUuid}/stations")
    @Operation(summary = "Anzeigen alle Stationen zu einer Störungsinformation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Erfolgreich durchgeführt"),
            @ApiResponse(responseCode = "404", description = "Station konnte nicht gefunden werden")})
    @ResponseStatus(HttpStatus.OK)
    public List<StationDto> getStationsForFailureInformation(@PathVariable UUID failureInfoUuid) {
        return failureInformationStationService.findStationsByFailureInfo(failureInfoUuid);
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @PostMapping("/{failureInfoUuid}/stations")
    @Operation(summary = "Zuordnen einer Station zu einer Störungsinformation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Station erfolgreich zugeordnet"),
            @ApiResponse(responseCode = "500", description = "Konnte nicht durchgeführt werden")
    })
    public ResponseEntity<FailureInformationStationDto> assignStationToFailureInfo(
            @PathVariable UUID failureInfoUuid,
            @Validated @RequestBody StationDto stationDto) {

        FailureInformationStationDto savedFailureInfoStationDto = failureInformationStationService.insertFailureInfoStation(failureInfoUuid, stationDto);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .path("/{stationid}")
                .buildAndExpand(savedFailureInfoStationDto.getStationStationId())
                .toUri();
        return ResponseEntity.created(location).body(savedFailureInfoStationDto);
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @DeleteMapping("/{failureInfoUuid}/stations/{stationUuid}")
    @Operation(summary = "Löschen der Zuordnung einer Station zu einer Störungsinformation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Stationszuordnung erfolgreich gelöscht"),
            @ApiResponse(responseCode = "400", description = "Ungültige Anfrage"),
            @ApiResponse(responseCode = "404", description = "Nicht gefunden")
    })
    public void deleteFailureInfoStation(
            @PathVariable UUID failureInfoUuid,
            @PathVariable UUID stationUuid) {
        failureInformationStationService.deleteFailureInfoStation(failureInfoUuid, stationUuid);
    }
}
