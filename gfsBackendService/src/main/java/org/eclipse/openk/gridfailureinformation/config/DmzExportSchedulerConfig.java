/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.config;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.service.ExportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Log4j2
@Configuration
@EnableScheduling
@ConditionalOnProperty(prefix = "export-to-dmz", name = "enabled", havingValue = "true", matchIfMissing = false)
public class DmzExportSchedulerConfig {

    private static final String SCHEDULER_NAME = "DmzExport-Scheduler";

    private final ExportService exportService;

    @Value("${export-to-dmz.cron}")
    private String cronExpression;

    public DmzExportSchedulerConfig(ExportService exportService) {
        this.exportService = exportService;
    }

    @Scheduled(cron = "${export-to-dmz.cron}")
    public void scheduleTaskDmzExport() {
        log.trace("Executing" + SCHEDULER_NAME + " task: Exporting FailureInformations with Status 'published' to DMZ" );
        exportService.exportFailureInformationsToDMZ();
        log.trace("Finished " + SCHEDULER_NAME + " task: Exporting FailureInformations with Status 'published' to DMZ");
        log.trace("Executing" + SCHEDULER_NAME + " task: Exporting FE-Settings to DMZ" );
        exportService.exportFeSettingsToDMZ();
        log.trace("Finished " + SCHEDULER_NAME + " task: Exporting FE-Settings to DMZ");
    }
}
