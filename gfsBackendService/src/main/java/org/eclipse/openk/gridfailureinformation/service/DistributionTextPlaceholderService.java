/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionTextPlaceholderDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;

@Service
@Log4j2
public class DistributionTextPlaceholderService {
    private final DistributionTextPlaceholderDto placeholderDto = initPlaceholderDto();

    private DistributionTextPlaceholderDto initPlaceholderDto() {
        try {
            InputStream inputStream = new ClassPathResource("DistributionTextPlaceholder.json").getInputStream();
            return  new ObjectMapper().readValue(inputStream, DistributionTextPlaceholderDto.class);
        } catch (IOException e) {
            log.error("Error importing JSON-Message", e);
            return null;
        }
    }

    public DistributionTextPlaceholderDto getPlaceholder() {
        return placeholderDto;
    }

}
