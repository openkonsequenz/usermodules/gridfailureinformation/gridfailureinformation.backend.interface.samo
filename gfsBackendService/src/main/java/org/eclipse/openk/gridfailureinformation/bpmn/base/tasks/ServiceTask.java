/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.bpmn.base.tasks;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessException;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessSubject;

@Log4j2
public abstract class ServiceTask <T extends ProcessSubject> extends BaseTask<T> {
    protected ServiceTask(String description) {
        super(description);
    }

    @Override
    public void enterStep(ProcessSubject model) throws ProcessException {
        log.debug("Enter: \""+getDescription()+"\"");
        this.leaveStep( model );
    }

    @Override
    protected void onEnterStep(ProcessSubject model) throws ProcessException {
        // implement empty
    }

    @Override
    protected void onRecover(T model) throws ProcessException {
        enterStep( model );
    }
}
