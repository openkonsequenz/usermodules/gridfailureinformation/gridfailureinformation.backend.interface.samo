/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.api;


import org.eclipse.openk.gridfailureinformation.model.JwtToken;
import org.eclipse.openk.gridfailureinformation.model.LoginCredentials;
import org.eclipse.openk.gridfailureinformation.viewmodel.UserModulePortalDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

@FeignClient(name = "${services.authNAuth.name}", url = "${services.authNAuth.url}")
public interface AuthNAuthApi {

    @GetMapping(value= "/portal/rest/beservice/checkAuth")
    feign.Response isTokenValid(@RequestHeader("Authorization") String token);

    @PostMapping( value="/portal/rest/beservice/login")
    JwtToken login(@RequestBody LoginCredentials loginCredentials);

    @GetMapping( value="/portal/rest/beservice/logout")
    feign.Response logout(@RequestHeader("Authorization") String token);

    @GetMapping( value="/portal/rest/beservice/userModulesForUser")
    List<UserModulePortalDto> userModulesForUser(@RequestHeader("Authorization") String token);

}
