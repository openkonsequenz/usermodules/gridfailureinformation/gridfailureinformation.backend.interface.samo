/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */

package org.eclipse.openk.gridfailureinformation.repository;

import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformationPublicationChannel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FailureInformationPublicationChannelRepository extends JpaRepository<TblFailureInformationPublicationChannel, Long > {
    List<TblFailureInformationPublicationChannel> findByTblFailureInformation(TblFailureInformation tblFailureInformation);
    int countByTblFailureInformationAndPublicationChannel(TblFailureInformation tblFailureInformation, String publicationChannel);
    Optional<TblFailureInformationPublicationChannel> findByTblFailureInformationAndPublicationChannel(TblFailureInformation tblFailureInformation, String publicationChannel);
    void deleteByTblFailureInformationAndPublicationChannel(TblFailureInformation tblFailureInformation, String publicationChannel);

    void deleteByTblFailureInformation(TblFailureInformation tblFailureInformation);
}
