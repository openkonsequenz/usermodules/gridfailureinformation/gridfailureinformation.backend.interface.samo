/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Data;

import java.util.Date;

@Data
@Entity
@Table( name = "HTBL_FAILINFO_STATION")
public class HtblFailureInformationStation {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,  generator = "HTBL_FAILINFO_STATION_ID_SEQ")
    @SequenceGenerator(name = "HTBL_FAILINFO_STATION_ID_SEQ", sequenceName = "HTBL_FAILINFO_STATION_ID_SEQ", allocationSize = 1)
    @Column(name = "hid", updatable = false)
    private Long hid;
    private Date hdate;

    private Long fkTblFailureInformation;
    private Long versionNumber;
    private String stationStationId;

}
