/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.exceptions.NotFoundException;
import org.eclipse.openk.gridfailureinformation.mapper.FailureInformationMapper;
import org.eclipse.openk.gridfailureinformation.mapper.HistFailureInformationStationMapper;
import org.eclipse.openk.gridfailureinformation.mapper.StationMapper;
import org.eclipse.openk.gridfailureinformation.model.HtblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.HtblFailureInformationStation;
import org.eclipse.openk.gridfailureinformation.model.TblFailinfoStation;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblStation;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationStationRepository;
import org.eclipse.openk.gridfailureinformation.repository.HistFailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.HistFailureInformationStationRepository;
import org.eclipse.openk.gridfailureinformation.repository.StationRepository;
import org.eclipse.openk.gridfailureinformation.viewmodel.HistFailureInformationStationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.StationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class HistFailureInformationStationService {
    private final StationRepository stationRepository;

    private final StationMapper stationMapper;

    private final HistFailureInformationRepository histFailureInformationRepository;

    private final HistFailureInformationStationRepository histFailureInformationStationRepository;

    private final FailureInformationStationRepository failureInformationStationRepository;

    private final HistFailureInformationStationMapper histFailureInformationStationMapper;

    public HistFailureInformationStationService(StationRepository stationRepository, StationMapper stationMapper, HistFailureInformationRepository histFailureInformationRepository, HistFailureInformationStationRepository histFailureInformationStationRepository, FailureInformationStationRepository failureInformationStationRepository, HistFailureInformationStationMapper histFailureInformationStationMapper) {
        this.stationRepository = stationRepository;
        this.stationMapper = stationMapper;
        this.histFailureInformationRepository = histFailureInformationRepository;
        this.histFailureInformationStationRepository = histFailureInformationStationRepository;
        this.failureInformationStationRepository = failureInformationStationRepository;
        this.histFailureInformationStationMapper = histFailureInformationStationMapper;
    }


    public List<StationDto> findHistStationsByFailureInfoAndVersionNumber(UUID failureInfoUuid, Long versionNumber) {
        List<HtblFailureInformation> htblFailureInformation = histFailureInformationRepository.findByUuidAndVersionNumber(failureInfoUuid, versionNumber);

        if (htblFailureInformation.isEmpty()) {
            throw new NotFoundException();
        }

        List<HtblFailureInformationStation> htblFailureInfoStationList = histFailureInformationStationRepository.findByFkTblFailureInformationAndVersionNumber(htblFailureInformation.get(0).getId(), versionNumber);
        List<TblStation> stationList = new ArrayList<>();

        for (HtblFailureInformationStation histFailureInfoStation: htblFailureInfoStationList) {
            stationList.add(stationRepository.findByStationId(histFailureInfoStation.getStationStationId()).orElseThrow(() -> new NotFoundException("station.id.not.existing")));
        }
        return stationList.stream()
                .map( stationMapper::toStationDto )
                .collect(Collectors.toList());
    }


    public List<HistFailureInformationStationDto> insertHistFailureInfoStationsForGfi(TblFailureInformation failureInformation) {
        List<TblFailinfoStation> fiStations =  failureInformationStationRepository.findByFailureInformationId(failureInformation.getId());
        List<HtblFailureInformationStation> savedFailureInformationStations = new ArrayList<>();

        if (!fiStations.isEmpty()) {
            for (TblFailinfoStation tblFiStation : fiStations) {
                HtblFailureInformationStation htblFailureInfoStationToSave = new HtblFailureInformationStation();

                htblFailureInfoStationToSave.setFkTblFailureInformation(failureInformation.getId());
                htblFailureInfoStationToSave.setVersionNumber(failureInformation.getVersionNumber());
                htblFailureInfoStationToSave.setStationStationId(tblFiStation.getStationStationId());
                htblFailureInfoStationToSave.setHdate(new Date());

                savedFailureInformationStations.add(histFailureInformationStationRepository.save(htblFailureInfoStationToSave));
            }
        }
        return savedFailureInformationStations.stream()
                .map( histFailureInformationStationMapper::toHistFailureInfoStationDto )
                .collect(Collectors.toList());

    }

}
