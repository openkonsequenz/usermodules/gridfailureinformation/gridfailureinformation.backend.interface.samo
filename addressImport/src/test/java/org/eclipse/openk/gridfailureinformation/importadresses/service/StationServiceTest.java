/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.importadresses.service;

import org.eclipse.openk.gridfailureinformation.importadresses.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.importadresses.mapper.StationMapper;
import org.eclipse.openk.gridfailureinformation.importadresses.model.TblStation;
import org.eclipse.openk.gridfailureinformation.importadresses.repository.StationRepository;
import org.eclipse.openk.gridfailureinformation.importadresses.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.importadresses.viewmodel.StationDto;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class StationServiceTest {
    @Autowired
    @SpyBean
    private StationService stationService;

    @Autowired
    private StationMapper stationMapper;

    @Autowired
    private StationRepository stationRepository;

    @Test
    public void shouldDeleteAllStationsProperly() {
        stationService.deleteAllStations();
        verify(stationRepository, times(1)).deleteAllInBatch();
    }

    @Test
    public void shouldInsertStationProperly() {
        StationDto stationDto = MockDataHelper.mockStationDto(1);
        stationDto.setUuid(null);

        when(stationRepository.findByStationId(any(String.class))).thenReturn(Optional.empty());
        when(stationRepository.save(any(TblStation.class)))
                .then((Answer<TblStation>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblStation) args[0];
                });

        StationDto stationDtoSaved = stationService.updateOrInsertStationByG3efid(stationDto);

        assertEquals(stationDtoSaved.getLatitude(), stationDto.getLatitude());
        assertEquals(stationDtoSaved.getLongitude(), stationDto.getLongitude());
        assertNotNull(stationDtoSaved.getUuid());
    }

    @Test
    public void shouldUpdateAdressesProperly() {
        StationDto stationDto = MockDataHelper.mockStationDto(1);
        TblStation tblStation = stationMapper.toTableStation(stationDto);


        when(stationRepository.findByStationId(any(String.class))).thenReturn(Optional.of(tblStation));
        when(stationRepository.save(any(TblStation.class)))
                .then((Answer<TblStation>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblStation) args[0];
                });

        StationDto stationDtoSaved = stationService.updateOrInsertStationByG3efid(stationDto);

        assertEquals(stationDtoSaved.getLatitude(), stationDto.getLatitude());
        assertEquals(stationDtoSaved.getLongitude(), stationDto.getLongitude());
    }

    @Test
    public void shouldThrowErrorOnUpdateAdresses() {
        StationDto stationDto = MockDataHelper.mockStationDto(1);
        TblStation tblStation = stationMapper.toTableStation(stationDto);

        when(stationRepository.findByStationId(any(String.class))).thenReturn(Optional.of(tblStation));
        when(stationRepository.save(any(TblStation.class)))
                .thenThrow(new IllegalArgumentException());

        StationDto stationDtoSaved = stationService.updateOrInsertStationByG3efid(stationDto);
        assertNotNull(stationDtoSaved);

    }
}
