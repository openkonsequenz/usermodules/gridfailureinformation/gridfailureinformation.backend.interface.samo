/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.importadresses.service;

import org.eclipse.openk.gridfailureinformation.importadresses.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.importadresses.viewmodel.AddressDto;
import org.eclipse.openk.gridfailureinformation.importadresses.viewmodel.StationDto;
import org.junit.jupiter.api.Test;
import org.powermock.reflect.Whitebox;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import javax.xml.ws.http.HTTPException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
class AdressImportServiceTest {
    @Autowired
    @SpyBean
    private AddressService addressService;

    @Autowired
    @SpyBean
    private StationService stationService;

    @Autowired
    @SpyBean
    private AddressImportService addressImportService;


    @Test
    void shouldNotImportIfAlreadyProcessing() {
        boolean b = Whitebox.getInternalState(addressImportService, "importInWork");
        Whitebox.setInternalState(addressImportService, "importInWork", true);

        assertThrows(HTTPException.class, () -> addressImportService.importAddresses(false) );

        Whitebox.setInternalState(addressImportService, "importInWork", b);
    }


    @Test
    void shouldImportAndInsertProperly() {
        addressImportService.importAddresses(true);

        verify(addressService, times(1)).deleteAllAddresses();
        verify(stationService, times(1)).deleteAllStations();
        verify(addressService, times(153)).insertAddress(any(AddressDto.class));
        verify(stationService, times(11)).insertStation(any(StationDto.class));
        verify(addressService, times(128)).updateOrInsertAddressByG3efid(any(AddressDto.class));
    }

    @Test
    void shouldImportAndInsertProperlyWithoutClean() {
        addressImportService.importAddresses(false);
        verify(addressService, times(0)).deleteAllAddresses();
        verify(stationService, times(0)).deleteAllStations();
        verify(addressService, times(153)).updateOrInsertAddressByG3efid(any(AddressDto.class));
        verify(stationService, times(11)).updateOrInsertStationByG3efid(any(StationDto.class));
    }


    @Test
    void shouldNotImportDueToMissingFiles() {
        String addressFile = Whitebox.getInternalState(addressImportService, "fileAddresses");
        String filePowerConnections = Whitebox.getInternalState(addressImportService, "filePowerConnections");
        String fileWaterConnections = Whitebox.getInternalState(addressImportService, "fileWaterConnections");
        String fileGasConnections = Whitebox.getInternalState(addressImportService, "fileGasConnections");
        String fileDistrictheatingConnections = Whitebox.getInternalState(addressImportService, "fileDistrictheatingConnections");
        String fileTelecommunicationConnections = Whitebox.getInternalState(addressImportService, "fileTelecommunicationConnections");
        Whitebox.setInternalState(addressImportService, "fileAddresses", "Not here");
        Whitebox.setInternalState(addressImportService, "filePowerConnections", "Not here");
        Whitebox.setInternalState(addressImportService, "fileWaterConnections", "Not here");
        Whitebox.setInternalState(addressImportService, "fileGasConnections", "Not here");
        Whitebox.setInternalState(addressImportService, "fileDistrictheatingConnections", "Not here");
        Whitebox.setInternalState(addressImportService, "fileTelecommunicationConnections", "Not here");


        addressImportService.importAddresses(false);
        verify(addressService, times(0)).deleteAllAddresses();
        verify(stationService, times(0)).deleteAllStations();

        Whitebox.setInternalState(addressImportService, "fileAddresses", addressFile);
        Whitebox.setInternalState(addressImportService, "filePowerConnections", filePowerConnections);
        Whitebox.setInternalState(addressImportService, "fileWaterConnections", fileWaterConnections);
        Whitebox.setInternalState(addressImportService, "fileGasConnections", fileGasConnections);
        Whitebox.setInternalState(addressImportService, "fileDistrictheatingConnections", fileDistrictheatingConnections);
        Whitebox.setInternalState(addressImportService, "fileTelecommunicationConnections", fileTelecommunicationConnections);
    }
}
