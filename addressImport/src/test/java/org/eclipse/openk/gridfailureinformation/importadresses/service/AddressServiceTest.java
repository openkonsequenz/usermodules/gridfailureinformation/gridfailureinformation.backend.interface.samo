/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.importadresses.service;

import org.eclipse.openk.gridfailureinformation.importadresses.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.importadresses.mapper.AddressMapper;
import org.eclipse.openk.gridfailureinformation.importadresses.model.TblAddress;
import org.eclipse.openk.gridfailureinformation.importadresses.repository.AddressRepository;
import org.eclipse.openk.gridfailureinformation.importadresses.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.importadresses.viewmodel.AddressDto;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
class AddressServiceTest {
    @Autowired
    private AddressService addressService;

    @Autowired
    private AddressMapper addressMapper;

    @Autowired
    private AddressRepository addressRepository;

    @Test
    void shouldDeleteAllAddressesProperly() {
        addressService.deleteAllAddresses();
        verify(addressRepository, times(1)).deleteAllInBatch();
    }

    @Test
    void shouldInsertAdressesProperly() {
        AddressDto addressDto = MockDataHelper.mockAddressDto(1);
        addressDto.setUuid(null);

        when(addressRepository.findByG3efid(any(Long.class))).thenReturn(Optional.empty());
        when(addressRepository.save(any(TblAddress.class)))
                .then((Answer<TblAddress>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblAddress) args[0];
                });

        AddressDto addressDtoSaved = addressService.updateOrInsertAddressByG3efid(addressDto);

        assertEquals(addressDtoSaved.getCommunity(), addressDto.getCommunity());
        assertEquals(addressDtoSaved.getLatitude(), addressDto.getLatitude());
        assertEquals(addressDtoSaved.getLongitude(), addressDto.getLongitude());
        assertNotNull(addressDtoSaved.getUuid());
    }

    @Test
    void shouldUpdateAddressesProperly() {
        AddressDto addressDto = MockDataHelper.mockAddressDto(1);
        addressDto.setPowerConnection(true);
        addressDto.setWaterConnection(true);
        addressDto.setGasConnection(true);
        addressDto.setDistrictheatingConnection(true);
        addressDto.setTelecommConnection(true);
        TblAddress tblAddress = addressMapper.toTableAddress(addressDto);


        when(addressRepository.findByG3efid(any(Long.class))).thenReturn(Optional.of(tblAddress));
        when(addressRepository.save(any(TblAddress.class)))
                .then((Answer<TblAddress>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblAddress) args[0];
                });

        AddressDto addressDtoSaved = addressService.updateOrInsertAddressByG3efid(addressDto);

        assertEquals(addressDtoSaved.getCommunity(), addressDto.getCommunity());
        assertEquals(addressDtoSaved.getLatitude(), addressDto.getLatitude());
        assertEquals(addressDtoSaved.getLongitude(), addressDto.getLongitude());
        assertTrue(addressDtoSaved.isPowerConnection());
        assertTrue(addressDtoSaved.isGasConnection());
        assertTrue(addressDtoSaved.isWaterConnection());
        assertTrue(addressDtoSaved.isDistrictheatingConnection());
        assertTrue(addressDtoSaved.isTelecommConnection());
    }

    @Test
    void shouldThrowErrorOnUpdateAddresses() {
        AddressDto addressDto = MockDataHelper.mockAddressDto(1);
        TblAddress tblAddress = addressMapper.toTableAddress(addressDto);

        when(addressRepository.findByG3efid(any(Long.class))).thenReturn(Optional.of(tblAddress));
        when(addressRepository.save(any(TblAddress.class)))
                .thenThrow(new IllegalArgumentException());

        AddressDto addressDtoSaved = addressService.updateOrInsertAddressByG3efid(addressDto);
        assertNotNull(addressDtoSaved);
    }
}
