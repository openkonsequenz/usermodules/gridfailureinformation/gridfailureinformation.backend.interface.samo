#!/bin/sh

# Ermittle das Verzeichnis, in dem sich das Shell-Script befindet
ROOT_DIR=$(cd "$(dirname "$0")" && pwd)

# portainer
docker-compose -f "$ROOT_DIR/infrastructure/portainer/docker-compose.yml" up -d
echo "portainer port: 9000"

# dozzle
docker-compose -f "$ROOT_DIR/infrastructure/dozzle/docker-compose.yml" up -d
echo "dozzle port: 9999"
