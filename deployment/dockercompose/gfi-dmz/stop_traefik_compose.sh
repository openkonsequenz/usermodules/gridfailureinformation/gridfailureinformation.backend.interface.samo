#!/bin/sh

# Ermittle das Verzeichnis, in dem sich das Shell-Script befindet
ROOT_DIR=$(cd "$(dirname "$0")" && pwd)

# SIT
docker-compose -f "$ROOT_DIR/traefik/docker-compose.yml" down
