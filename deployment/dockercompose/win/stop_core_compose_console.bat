@echo off

rem Setzen Sie das Root-Verzeichnis, in dem sich die Docker-Compose-Projekte befinden
set "ROOT_DIR=%~dp0.."

rem Start Projekt 1 in einer neuen Konsole
start "postgresql" cmd /c "cd /d %ROOT_DIR%/infrastructure/db/postgresql && docker-compose down"

rem Start Projekt 2 in einer neuen Konsole
start "traefik" cmd /c "cd /d %ROOT_DIR%/infrastructure/traefik && docker-compose down"

rem Start Projekt 3 in einer neuen Konsole
start "portal" cmd /c "cd /d %ROOT_DIR%/portal && docker-compose down"

start "cbd" cmd /c "cd /d %ROOT_DIR%/cbd && docker-compose down"