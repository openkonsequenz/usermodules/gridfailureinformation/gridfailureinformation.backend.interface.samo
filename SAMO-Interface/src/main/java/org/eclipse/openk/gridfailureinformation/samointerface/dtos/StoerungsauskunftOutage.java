/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.samointerface.dtos;

import lombok.Data;

import java.io.Serializable;

@Data
public class StoerungsauskunftOutage implements Serializable {

    private String id;
    private String operatorID;
    private String type;
    private Integer origin;
    private Integer geoType;
    private String date;
    private String estimatedEnding;
    private String postcode;
    private String city;
    private String district;
    private String ags;
    private Integer radius;
    private String coordinates;
    private String liveInfo;
    private Integer social;
    private String socialText;

}
