/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.samointerface.service;

//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.commons.io.FileUtils;
import org.eclipse.openk.gridfailureinformation.samointerface.SarisInterfaceApplication;
import org.eclipse.openk.gridfailureinformation.samointerface.config.TestConfiguration;
//import org.eclipse.openk.gridfailureinformation.samointerface.dtos.ForeignFailureMessageDto;
//import org.eclipse.openk.gridfailureinformation.samointerface.dtos.StoerungsauskunftUserNotification;
//import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.api.StoerungsauskunftApi;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.SpyBean;
//import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.io.File;
import java.io.IOException;
//import java.io.InputStream;
//import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.ArgumentMatchers.anyInt;
//import static org.mockito.Mockito.spy;
//import static org.mockito.Mockito.times;
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.when;

@SpringBootTest(classes = SarisInterfaceApplication.class)
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class ImportServiceTest {
    @Autowired
    private ImportService importService;

//    @Autowired
//    private ObjectMapper objectMapper;
//
//    @SpyBean
//    private StoerungsauskunftApi stoerungsauskunftApi;

    @Test
    void shouldImportSAMOOutage() throws IOException {
        String dateiname = "SAMO-ImportJsonFile.json";
        File file = new File("src/test/resources/" + dateiname);
        assertTrue(file.exists());

        String demoOutag = FileUtils.readFileToString(file, "UTF-8");

        importService.importSAMOOutage(demoOutag);
    }

    /*@Test
    public void shouldImportUserNotification() throws IOException {
        ImportService importExportServicSpy = spy(importService);

        InputStream is = new ClassPathResource("UsernotificationJsonResponse.json").getInputStream();
        List<StoerungsauskunftUserNotification> userNotificationList =
                objectMapper.readValue(is, new TypeReference<>() {
                });
        when(stoerungsauskunftApi.getUserNotification(anyInt())).thenReturn(userNotificationList);

        importExportServicSpy.importUserNotifications();

        verify(importExportServicSpy, times(userNotificationList.size())).pushForeignFailure(any(ForeignFailureMessageDto.class));
    }

    @Test
    public void shoulImportUserNotificationMapperTest1() throws IOException {

        ImportService importExportServicSpy = spy(importService);

        InputStream is = new ClassPathResource("UsernotificationJsonResponse.json").getInputStream();
        List<StoerungsauskunftUserNotification> userNotificationList =
                objectMapper.readValue(is, new TypeReference<List<StoerungsauskunftUserNotification>>() {
                });
        when(stoerungsauskunftApi.getUserNotification(any(Integer.class))).thenReturn(userNotificationList);

        importExportServicSpy.importUserNotifications();

        verify(importExportServicSpy, times(userNotificationList.size())).pushForeignFailure(any(ForeignFailureMessageDto.class));
    }*/
}
