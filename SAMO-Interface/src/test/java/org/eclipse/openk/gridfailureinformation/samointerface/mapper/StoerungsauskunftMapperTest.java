package org.eclipse.openk.gridfailureinformation.samointerface.mapper;

import org.eclipse.openk.gridfailureinformation.samointerface.SarisInterfaceApplication;
import org.eclipse.openk.gridfailureinformation.samointerface.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.samointerface.dtos.ForeignFailureDataDto;
import org.eclipse.openk.gridfailureinformation.samointerface.util.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.samointerface.dtos.StoerungsauskunftUserNotification;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = SarisInterfaceApplication.class)
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class StoerungsauskunftMapperTest {
    @Autowired
    private StoerungsauskunftMapper stoerungsauskunftMapper;

    @Test
    void shouldMapStoerungsauskunftUserNotificationToForeignFailureDataDto() {
        StoerungsauskunftUserNotification notification = MockDataHelper.mockStoerungsauskunftUserNotification();

        ForeignFailureDataDto dto = stoerungsauskunftMapper.toForeignFailureDataDto(notification);

        // dto.getFailureBegin() gets the date in the timezone of the running jvm, so we need to format the date explicitly at UTC
        ZonedDateTime actualDate = dto.getFailureBegin().toInstant().atZone(ZoneOffset.UTC);

        assertEquals(notification.getComment(), dto.getDescription());
        assertEquals("28.5.2020 14:49:11", notification.getDate());
        assertEquals("2020-05-28T12:49:11Z", stoerungsauskunftMapper.convertToUTC(notification.getDate()).toString());
        assertEquals(stoerungsauskunftMapper.convertToUTC(notification.getDate()), actualDate);
        assertEquals(notification.getLng(), dto.getLongitude().toString());
        assertEquals(notification.getLat(), dto.getLatitude().toString());
        assertEquals(notification.getStreet(), dto.getStreet());
        assertEquals(notification.getHouseNo(), dto.getHousenumber());
        assertEquals(notification.getCity(), dto.getCity());
    }
}
