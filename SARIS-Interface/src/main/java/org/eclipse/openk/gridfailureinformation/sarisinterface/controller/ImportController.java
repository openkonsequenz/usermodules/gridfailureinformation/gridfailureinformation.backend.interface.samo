/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.sarisinterface.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.sarisinterface.constants.Constants;
import org.eclipse.openk.gridfailureinformation.sarisinterface.service.ImportService;
import org.eclipse.openk.gridfailureinformation.sarisinterface.service.SarisWebservice;
import org.eclipse.openk.gridfailureinformation.sarisinterface.wsdl.ArrayOfViewGeplanteVU;
import org.eclipse.openk.gridfailureinformation.sarisinterface.wsdl.GetAktuelleGVUsInfoAllgemeinResponse;
import org.eclipse.openk.gridfailureinformation.sarisinterface.wsdl.ViewGeplanteVU;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Log4j2
@RestController
@RequestMapping("/saris")
public class ImportController {
    private final ImportService importService;

    private final SarisWebservice sarisWebservice;

    public ImportController(ImportService importService, SarisWebservice sarisWebservice) {
        this.importService = importService;
        this.sarisWebservice = sarisWebservice;
    }

    @GetMapping("/live-import-test")
    @Operation(summary = "Import einer externen Störungsinformation von SARIS zu SIT")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Störungsinformation erfolgreich importiert"),
            @ApiResponse(responseCode = "500", description = "Konnte nicht durchgeführt werden")
    })
    @ResponseStatus(HttpStatus.OK)
    public void liveImportUserNotification() {
      importService.importForeignFailures(Constants.SARIS_ELECTRICITY_BRANCH_ID, false, false);
      importService.importForeignFailures(Constants.SARIS_GAS_BRANCH_ID, false, false);
      importService.importForeignFailures(Constants.SARIS_WATER_BRANCH_ID, false, false);
    }

    @GetMapping("/import-test")
    @Operation(summary = "Import einer externen Störungsinformation von Störungsauskunft.de zu SIT")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Störungsinformation erfolgreich importiert"),
            @ApiResponse(responseCode = "500", description = "Konnte nicht durchgeführt werden")
    })
    @ResponseStatus(HttpStatus.OK)
    public void importUserNotification(@RequestParam(defaultValue = "true") boolean mocked,
        @RequestParam(defaultValue = "true") boolean test ) {
        importService.importForeignFailures(Constants.SARIS_ELECTRICITY_BRANCH_ID, test, mocked);
    }

    @GetMapping("/response-test")
    @Operation(summary = "Response-Test für SARIS")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Response von Saris erfolgreich"),
            @ApiResponse(responseCode = "500", description = "Konnte nicht durchgeführt werden")
    })
    @ResponseStatus(HttpStatus.OK)
    public void responsetest() {
        log.info("response-test");
        GetAktuelleGVUsInfoAllgemeinResponse response = sarisWebservice.getAktuelleGVU(Constants.SARIS_ELECTRICITY_BRANCH_ID,true);
        log.info("RESPONSE received import-test: " + response);
        ArrayOfViewGeplanteVU getAktuelleGVUsInfoAllgemeinResult = response.getGetAktuelleGVUsInfoAllgemeinResult();
        if (getAktuelleGVUsInfoAllgemeinResult != null) {
            List<ViewGeplanteVU> viewGeplanteVU = response.getGetAktuelleGVUsInfoAllgemeinResult().getViewGeplanteVU();
            for (ViewGeplanteVU geplanteVU : viewGeplanteVU) {
                log.info("VersorgungsunterbrechungID: " + geplanteVU.getVersorgungsunterbrechungID());
            }
        }
    }
}
