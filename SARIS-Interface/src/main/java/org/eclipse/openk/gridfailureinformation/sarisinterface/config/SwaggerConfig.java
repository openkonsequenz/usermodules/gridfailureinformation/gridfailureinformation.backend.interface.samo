package org.eclipse.openk.gridfailureinformation.sarisinterface.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import lombok.extern.log4j.Log4j2;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Log4j2
@Configuration
@Profile({"!test"})
public class SwaggerConfig {
    @Value("${swagger.baseUrl:}")
    public String baseUrl;

    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
                .info(new Info().title("SARIS-Interface")
                        .version("1.0.0")
                        .description("Grid-Failure-Information Project for openKONSEQUENZ"));
    }

    @Bean
    public GroupedOpenApi publicApi() {
        return GroupedOpenApi.builder()
                .group("saris")
                .pathsToMatch(baseUrl.concat("/**"))
                .build();
    }
}
