/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.sarisinterface.support;

import org.eclipse.openk.gridfailureinformation.sarisinterface.constants.Constants;
import org.eclipse.openk.gridfailureinformation.sarisinterface.dtos.FailureInformationDto;
import org.eclipse.openk.gridfailureinformation.sarisinterface.dtos.ForeignFailureDataDto;
import org.eclipse.openk.gridfailureinformation.sarisinterface.dtos.ForeignFailureMessageDto;
import org.eclipse.openk.gridfailureinformation.sarisinterface.wsdl.GetAktuelleGVUsInfoAllgemeinResponse;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class MockDataHelper {

    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

    private MockDataHelper() {}

    public static ForeignFailureMessageDto mockForeignFailureDto() throws ParseException {
        ForeignFailureMessageDto foreignFailureMessageDto = new ForeignFailureMessageDto();
        foreignFailureMessageDto.setMetaId("a4509482-3be2-4402-a9bd-ff91649b9c1e");
        foreignFailureMessageDto.setSource(Constants.SRC_SARIS);
        foreignFailureMessageDto.setDescription("Rohrbruch");

        ForeignFailureDataDto dataDto = new ForeignFailureDataDto();
        dataDto.setPlanned(true);
        dataDto.setFailureBegin(SIMPLE_DATE_FORMAT.parse("06.05.2020 12:30:00"));
        dataDto.setFailureEndPlanned(SIMPLE_DATE_FORMAT.parse("06.04.2021 13:30:00"));
        dataDto.setPostcode("55232");
        dataDto.setCity("Alzey");
        dataDto.setDescription("In Alzey ist eine PLZ/Gemeindestörung");
        foreignFailureMessageDto.setPayload(dataDto);

        return foreignFailureMessageDto;
    }

    public static ForeignFailureMessageDto mockForeignFailureDtoWithCoordinates() throws ParseException {
        ForeignFailureMessageDto foreignFailureMessageDto = new ForeignFailureMessageDto();
        foreignFailureMessageDto.setMetaId("a4509482-3be2-4402-a9bd-ff91649b9c1e");
        foreignFailureMessageDto.setSource(Constants.SRC_SARIS);
        foreignFailureMessageDto.setDescription("Rohrbruch");

        ForeignFailureDataDto dataDto = new ForeignFailureDataDto();
        dataDto.setPlanned(true);
        dataDto.setFailureBegin(SIMPLE_DATE_FORMAT.parse("06.05.2020 12:30:00"));
        dataDto.setFailureEndPlanned(SIMPLE_DATE_FORMAT.parse("06.04.2021 13:30:00"));
        dataDto.setPostcode("55232");
        dataDto.setCity("Alzey");
        dataDto.setDescription("In Alzey ist eine PLZ/Gemeindestörung");
        dataDto.setLatitude(new BigDecimal("49.7842378"));
        dataDto.setLongitude(new BigDecimal("8.0308092"));
        foreignFailureMessageDto.setPayload(dataDto);

        return foreignFailureMessageDto;
    }

    public static FailureInformationDto mockFailureInformationDto() {

        FailureInformationDto dto = new FailureInformationDto();
        dto.setUuid(UUID.randomUUID());
        dto.setVersionNumber(3L);
        dto.setResponsibility("Vatter Abraham");
        dto.setFailureBegin(Date.valueOf("2022-12-01"));
        dto.setFailureEndPlanned(Date.valueOf("2022-12-02"));
        dto.setFailureEndResupplied(Date.valueOf("2022-12-03"));


        dto.setStreet("Budenweg");
        dto.setDistrict("West");
        dto.setCity("Waldau");

        dto.setStationId("224488-123bcd");
        dto.setStationDescription("Trafo 25");
        dto.setStationCoords("121,8855");
        dto.setLongitude(BigDecimal.valueOf(8.646280));
        dto.setLatitude(BigDecimal.valueOf(50.115618));

        dto.setRadiusId(UUID.randomUUID());
        dto.setRadius(50L);

        dto.setPublicationStatus("veröffentlicht");
        dto.setPublicationFreetext("Kabel aus Steckdose gerissen");

        dto.setExpectedReasonId(UUID.randomUUID());
        dto.setExpectedReasonText("Kabelfehler Niederspannung");

        dto.setFailureClassificationId(UUID.randomUUID());
        dto.setFailureClassification("FailClazz");

        dto.setFailureTypeId(UUID.randomUUID());
        dto.setFailureType("FailTypo");

        dto.setStatusInternId(UUID.randomUUID());
        dto.setStatusIntern("NEW");

        dto.setStatusExternId(UUID.randomUUID());
        dto.setStatusExtern("CLOSED");

        dto.setBranchId(UUID.randomUUID());
        dto.setBranch("G");
        dto.setBranchColorCode("#fdea64");

        dto.setCreateDate(Date.valueOf("2020-05-08"));
        dto.setCreateUser("weizenkeimk");
        dto.setModDate(Date.valueOf("2020-05-23"));
        dto.setModUser("schlonzh");

        return dto;
    }

    public static List<ArrayList<BigDecimal>> mockPolygonCoordinatesList(){

        List<ArrayList<BigDecimal>> cordinatesList = new LinkedList<>();

        ArrayList<BigDecimal> firstCoordinate = new ArrayList<>();
        firstCoordinate.add(new BigDecimal(53.5));
        firstCoordinate.add(new BigDecimal(2.7));

        ArrayList<BigDecimal> secondCoordinate = new ArrayList<>();
        secondCoordinate.add(new BigDecimal(52.8));
        secondCoordinate.add(new BigDecimal(2.1));

        ArrayList<BigDecimal> thirdCoordinate = new ArrayList<>();
        thirdCoordinate.add(new BigDecimal(53.66));
        thirdCoordinate.add(new BigDecimal(2.33));

        ArrayList<BigDecimal> fourthCoordinate = new ArrayList<>();
        fourthCoordinate.add(new BigDecimal(52.9));
        fourthCoordinate.add(new BigDecimal(2.0));

        ArrayList<BigDecimal> fifthCoordinate = new ArrayList<>();
        fifthCoordinate.add(new BigDecimal(53.45));
        fifthCoordinate.add(new BigDecimal(2.77));

        cordinatesList.add(firstCoordinate);
        cordinatesList.add(secondCoordinate);
        cordinatesList.add(thirdCoordinate);
        cordinatesList.add(fourthCoordinate);
        cordinatesList.add(fifthCoordinate);

        return cordinatesList;
    }

    public static GetAktuelleGVUsInfoAllgemeinResponse getMockedSarisResponse(String xmlResponseFile) {
        GetAktuelleGVUsInfoAllgemeinResponse response = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(GetAktuelleGVUsInfoAllgemeinResponse.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            File file = new File(xmlResponseFile);
            response = (GetAktuelleGVUsInfoAllgemeinResponse) jaxbUnmarshaller.unmarshal(file);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return response;
    }

}
